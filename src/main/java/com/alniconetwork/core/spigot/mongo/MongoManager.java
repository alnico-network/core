package com.alniconetwork.core.spigot.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import com.alniconetwork.core.spigot.util.Logger;

import java.util.Collections;

@Getter
public class MongoManager extends Manager {

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;

    public MongoManager(ManagerHandler managerHandler) {
        super(managerHandler);

        establishConnection();
    }

    private void establishConnection() {
        String ip = managerHandler.getPlugin().getConfig().getString("mongo.ip");
        int port = managerHandler.getPlugin().getConfig().getInt("mongo.port");
        String database = managerHandler.getPlugin().getConfig().getString("mongo.database");
        boolean usePassword = managerHandler.getPlugin().getConfig().getBoolean("mongo.use-password");
        String username = managerHandler.getPlugin().getConfig().getString("mongo.username");
        String password = managerHandler.getPlugin().getConfig().getString("mongo.password");

        try {
            if (usePassword) {
                mongoClient = new MongoClient(new ServerAddress(ip, port), Collections.singletonList(MongoCredential.createCredential(username, database, password.toCharArray())));
            } else {
                mongoClient = new MongoClient(new ServerAddress(ip, port));
            }
            mongoDatabase = mongoClient.getDatabase(database);

            if (!collectionExists("ranks")) mongoDatabase.createCollection("ranks");
            if (!collectionExists("bans")) mongoDatabase.createCollection("bans");
            if (!collectionExists("mutes")) mongoDatabase.createCollection("mutes");
            if (!collectionExists("kicks")) mongoDatabase.createCollection("kicks");
            if (!collectionExists("playerdata")) mongoDatabase.createCollection("playerdata");
            if (!collectionExists("grants")) mongoDatabase.createCollection("grants");
            if (!collectionExists("blacklists")) mongoDatabase.createCollection("blacklists");

            mongoClient.getAddress();
            Logger.success("Successfully established Mongo connection.");
        } catch (Exception ex) {
            Logger.error("Could not establish Mongo connection.");
        }
    }

    public MongoCollection getCollection(String collection) {
        return mongoDatabase.getCollection(collection);
    }

    public boolean collectionExists(String collection) {
        boolean exists = false;
        for (String collections : mongoDatabase.listCollectionNames()) {
            if (collection.equalsIgnoreCase(collections)) exists = true;
        }
        return exists;
    }
}