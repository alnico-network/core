package com.alniconetwork.core.spigot.punishment.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.StringUtil;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class KickCommand {

    private String reason;

    private final CorePlugin plugin;

    @Command(name = "kick", permission = "core.command.kick")
    public void kick(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> [-s] [reason]");
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        reason = "";
        boolean silent = false;
        if (args.length() > 1) {

            reason = StringUtil.buildString(args.getArgs(), 1);

            silent = reason.contains("-s");
            reason = reason.replaceFirst("-s", "");
            reason = reason.replace("  ", " ");
            if (reason.startsWith(" ")) {
                reason = reason.replaceFirst(" ", "");
            }
        }
        if (reason.endsWith(" ")) {
            reason = StringUtil.replaceLast(reason, " ", "");
        }
        if (reason.replace(" ", "").equalsIgnoreCase("")) {
            reason = plugin.getConfig().getString("punishment.default-kick-reason");
        }

        MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("kicks");
        Punishment punishment = new Punishment(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null, Punishment.PunishmentType.KICK, System.currentTimeMillis(), "N/A", System.currentTimeMillis(), reason, false, null, "", 0);

        Document newDocument = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);
        newDocument.put("uuid", target.getUniqueId().toString());

        CompletableFuture.runAsync(() ->
                mongoCollection.insertOne(newDocument));

        CoreProfile targetProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);
        targetProfile.getPunishmentList().add(punishment);

        target.kickPlayer(CC.translate(this.plugin.getConfig().getString("punishment.kick-format")
                .replace("%reason%", punishment.getReason())
                .replace("%expire%", TimeUtil.formatTimeMillis(punishment.getExpire() - System.currentTimeMillis(), false))));

        String broadcast = (silent ? Messages.KICK_BROADCAST_SILENT : Messages.KICK_BROADCAST).getMessage()
                .replace("%player%", args.getSender().getName())
                .replace("%player1%", target.getName())
                .replace("%reason%", reason);

        if (silent) {
            this.plugin.getServer().broadcast(broadcast, "core.silent");
            this.plugin.getServer().broadcast(broadcast, Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
        } else
            this.plugin.getServer().broadcastMessage(broadcast);
        return;
    }
}
