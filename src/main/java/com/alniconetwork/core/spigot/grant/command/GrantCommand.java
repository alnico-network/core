package com.alniconetwork.core.spigot.grant.command;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.grant.Grant;
import com.alniconetwork.core.spigot.grant.menu.GrantMenu;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.StringUtil;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class GrantCommand {

    private final CorePlugin plugin;

    private UUID uuid;
    private String name;

    @Command(name = "grant", permission = "core.command.grant")
    public void grant(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> [rank] [duration] [reason]");
            return;
        }
        uuid = null;
        name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        if (args.length() == 1) {
            if (!(args.getSender() instanceof Player)) {
                args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> <rank> [duration] [reason]");
                return;
            }
            Player player = args.getPlayer();
            GrantMenu grantMenu = new GrantMenu(name, 1);
            grantMenu.open(player);
            return;
        }
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(args.getArgs(1));
        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String reason = plugin.getConfig().getString("grant.default-reason");
        long time = -1L;
        if (args.length() > 2) {
            time = TimeUtil.parseTime(args.getArgs(2));

            reason = StringUtil.buildString(args.getArgs(), time == -1L ? 2 : 3);

            if (reason.startsWith(" ")) {
                reason = reason.replaceFirst(" ", "");
            }
        }
        Grant grant = new Grant(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null, System.currentTimeMillis(), rank.getName(), time == -1 ? "Forever" : TimeUtil.formatTimeMillis(time, true), System.currentTimeMillis() + time, reason, true);

        CompletableFuture.runAsync(() -> {
            MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("grants");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();

                Grant foundGrant = this.plugin.getManagerHandler().getGrantManager().fromDocument(foundDocument);
                if (foundGrant.isActive()) foundGrant.setActive(false);

                Document document = this.plugin.getManagerHandler().getGrantManager().toDocument(foundGrant);
                document.put("uuid", uuid.toString());

                mongoCollection.deleteOne(foundDocument);
                mongoCollection.insertOne(document);
            }
            Document document = this.plugin.getManagerHandler().getGrantManager().toDocument(grant);
            document.put("uuid", uuid.toString());
            mongoCollection.insertOne(document);
        });
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("player", name);
        messageMap.put("rank", rank.getName());

        plugin.getManagerHandler().getRedisManager().send("grant" ,messageMap);

        args.getSender().sendMessage(Messages.GRANTED_RANK.getMessage()
                .replace("%player%", name)
                .replace("%rank%", rank.getName())
                .replace("%time%", time == -1 ? "forever" : TimeUtil.formatTimeMillis(time, false))
                .replace("%reason%", reason));
        return;
    }
}
