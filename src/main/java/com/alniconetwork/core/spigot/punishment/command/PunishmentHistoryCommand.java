package com.alniconetwork.core.spigot.punishment.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.punishment.menu.PunishmentHistoryMenu;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

@AllArgsConstructor
public class PunishmentHistoryCommand {

    private CorePlugin plugin;

    @Command(name = "punishmenthistory", aliases = {"punishments", "bans", "baninfo", "mutes", "muteinfo", "blacklists", "blacklistinfo", "kicks", "kickinfo"}, permission = "core.command.punishmenthistory", inGameOnly = true)
    public void punishmentHistory(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player>");
            return;
        }
        UUID uuid = null;
        String name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        Player player = args.getPlayer();

        PunishmentHistoryMenu punishmentHistoryMenu = new PunishmentHistoryMenu(name, uuid, target != null, Punishment.PunishmentFilter.NONE, 1);
        punishmentHistoryMenu.open(player);
        return;
    }
}
