package com.alniconetwork.core.spigot.punishment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class Punishment {

    private UUID addedBy;
    private PunishmentType punishmentType;
    private long date;
    private String time;
    private long expire;
    private String reason;

    @Setter
    private boolean active;
    @Setter
    private UUID removedBy;
    @Setter
    private String removalReason;
    @Setter
    private long removalDate;

    public enum PunishmentFilter {

        NONE, BANS, BLACKLISTS, MUTES, KICKS
    }

    public enum PunishmentType {

        BAN, BLACKLIST, MUTE, KICK
    }

}