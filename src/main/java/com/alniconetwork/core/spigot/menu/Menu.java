package com.alniconetwork.core.spigot.menu;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.reflection.InventoryReflection;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

@Getter
public class Menu implements Listener {

    private final String name;
    private final int slots;
    private final Inventory inventory;

    @Setter
    private Player player;

    private boolean opened;

    public Menu(String name, int slots, boolean autoUpdate) {
        this.name = name;
        this.slots = slots;
        this.inventory = Bukkit.createInventory(null, slots, name);

        if (autoUpdate) {
            new BukkitRunnable() {
                public void run() {
                    if (!opened) this.cancel();
                    else
                        build();
                }
            }.runTaskTimerAsynchronously(CorePlugin.getInstance(), 0L, 10L);
        }

        Bukkit.getPluginManager().registerEvents(this, CorePlugin.getInstance());
    }

    public void build() {
    }

    public void open(Player player) {
        this.player = player;
        opened = true;
        build();
        InventoryReflection.openMenu(player, inventory);
    }

    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (event.getCurrentItem() != null && event.getClickedInventory() != null && event.getClickedInventory().equals(inventory) && event.getSlotType() == InventoryType.SlotType.CONTAINER && player == this.player) {
            MenuEvent menuEvent = new MenuEvent(event.isLeftClick(), event.isRightClick(), event.isShiftClick());
            this.onClick(player, event.getCurrentItem(), event.getSlot(), menuEvent);

            event.setCancelled(menuEvent.isCancelled());

        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().equals(inventory)) {
            opened = false;
        }
    }
}
