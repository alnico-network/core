package com.alniconetwork.core.spigot.grant.menu;

import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GrantDurationMenu extends Menu {

    private String name;
    private Rank rank;
    private String time = "";

    private boolean settingCustomDuration;

    public GrantDurationMenu(String name, Rank rank) {
        super("Select a grant duration", 9, false);

        this.name = name;
        this.rank = rank;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        inventory.setItem(0,
                new ItemBuilder(Material.LIME_STAINED_GLASS_PANE)
                        .setName(CC.GREEN + "Permanent")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " permanently")
                        .toItemStack());

        inventory.setItem(1,
                new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE)
                        .setName(CC.YELLOW + "30 Days")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " for 30 days")
                        .toItemStack());

        inventory.setItem(2,
                new ItemBuilder(Material.ORANGE_STAINED_GLASS_PANE)
                        .setName(CC.GOLD + "14 Days")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " for 14 days")
                        .toItemStack());

        inventory.setItem(3,
                new ItemBuilder(Material.RED_STAINED_GLASS_PANE)
                        .setName(CC.RED + "7 Days")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " for 7 days")
                        .toItemStack());

        inventory.setItem(4,
                new ItemBuilder(Material.MAGENTA_STAINED_GLASS_PANE)
                        .setName(CC.PINK + "1 Day")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " for 1 day")
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.LIGHT_BLUE_STAINED_GLASS_PANE)
                        .setName(CC.AQUA + "Custom Duration")
                        .setLore(CC.GRAY + "Grant " + name + " " + rank.getName() + " for a custom period")
                        .toItemStack());
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        if (currentItem.getType() == Material.AIR) return;

        switch (slot) {
            case 1: {
                time = "30d";
                break;
            }
            case 2: {
                time = "14d";
                break;
            }
            case 3: {
                time = "7d";
                break;
            }
            case 4: {
                time = "1d";
                break;
            }
            case 8: {
                settingCustomDuration = true;

                player.closeInventory();
                player.sendMessage(Messages.SETTING_CUSTOM_DURATION.getMessage()
                        .replace("%player%", name)
                        .replace("%rank%", rank.getName()));
                break;
            }
        }
        if (slot != 8) {
            openReasonMenu();
        }
        menuEvent.setCancelled(true);
    }

    @EventHandler
    public void onChat(PlayerChatEvent event) {
        Player player = event.getPlayer();

        if (player != this.getPlayer()) return;

        if (settingCustomDuration) {
            event.setCancelled(true);

            time = event.getMessage();

            if (TimeUtil.parseTime(time) == -1L) {
                time = "";
            }
            openReasonMenu();
            settingCustomDuration = false;
            return;
        }
    }

    private void openReasonMenu() {
        GrantReasonMenu grantReasonMenu = new GrantReasonMenu(name, rank, time);
        grantReasonMenu.open(getPlayer());
    }
}
