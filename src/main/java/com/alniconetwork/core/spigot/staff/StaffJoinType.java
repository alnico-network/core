package com.alniconetwork.core.spigot.staff;

public enum StaffJoinType {

    JOIN, QUIT, SWITCH
}
