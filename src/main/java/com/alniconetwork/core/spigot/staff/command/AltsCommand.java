package com.alniconetwork.core.spigot.staff.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Strings;
import org.bson.Document;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class AltsCommand {

    private final CorePlugin plugin;

    private String name;
    private UUID uuid;
    private List<String> ips;

    @Command(name = "alts", permission = "core.command.alts")
    public void alts(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player>");
            return;
        }
        uuid = null;
        List<String> altNames = new ArrayList<>();
        name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        args.getSender().sendMessage(CC.GRAY + "Grabbing alts...");
        MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("playerdata");
        if (target != null) {
            CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);
            ips = coreProfile.getIps();
        } else {
            CompletableFuture.runAsync(() -> {
                Document document = (Document) mongoCollection.find(Filters.eq("uuid", uuid.toString())).first();

                if (document == null) {
                    args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", name));
                    return;
                }
                ips = (List<String>) document.get("ips");
            });
        }
        CompletableFuture.runAsync(() -> {
            MongoCursor mongoCursor = mongoCollection.find().iterator();

            while (mongoCursor.hasNext()) {
                Document document = (Document) mongoCursor.next();

                List<String> ips = (List<String>) document.get("ips");

                boolean alt = false;
                for (String ip : this.ips) if (ips.contains(ip)) alt = true;

                if (alt) {
                    altNames.add(document.getString("username"));
                }
            }
            if(!altNames.isEmpty()) {
                args.getPlayer().sendMessage(Strings.join(altNames, ", "));
                altNames.clear();
                ips.clear();
            }
        });
        //TODO: Send alts message here
    }
}
