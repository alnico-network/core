package com.alniconetwork.core.spigot.util;

import org.bukkit.Bukkit;

public class Logger {

    public static void success(String message) {
        Bukkit.getConsoleSender().sendMessage(CC.GREEN + "[ANCore] " + message);
    }

    public static void error(String message) {
        Bukkit.getConsoleSender().sendMessage(CC.RED + "[ANCore] " + message);
    }
}
