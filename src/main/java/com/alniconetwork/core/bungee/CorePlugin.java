package com.alniconetwork.core.bungee;

import com.alniconetwork.core.bungee.events.ConnectionListeners;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Collection;
import java.util.logging.Level;

public class CorePlugin extends Plugin {


    @Getter
    private static CorePlugin instance;

    @Override
    public void onEnable() {
        instance = this;
        getProxy().registerChannel("an:channel");
        getProxy().getPluginManager().registerListener(this, new ConnectionListeners());
    }

    public void sendCustomData(ProxiedPlayer p, String data1, String data2)
    {
        Collection<ProxiedPlayer> networkPlayers = ProxyServer.getInstance().getPlayers();
        // perform a check to see if globally are no players
        if ( networkPlayers == null || networkPlayers.isEmpty() )
        {
            return;
        }

        if(data2 == "leave") {
            ProxiedPlayer random = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF( random.getServer().getInfo().getName() ); // the channel could be whatever you want
            out.writeUTF( data1 ); // this data could be whatever you want
            out.writeUTF( data2 ); // this data could be whatever you want
            random.getServer().getInfo().sendData("an:channel", out.toByteArray());
        } else {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF(p.getServer().getInfo().getName()); // the channel could be whatever you want
            out.writeUTF(data1); // this data could be whatever you want
            out.writeUTF(data2); // this data could be whatever you want

            p.getServer().getInfo().sendData("an:channel", out.toByteArray());
        }
    }
}
