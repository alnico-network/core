package com.alniconetwork.core.spigot;

import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Logger;
import org.bukkit.entity.Player;

public class CoreAPI {

    public static CoreProfile getCoreProfile(Player player) {
        return CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(player);
    }

    public static void setRank(Player player, Rank rank) {
        getCoreProfile(player).setRank(rank);
    }

    public static Rank getRank(Player player) {
        return getCoreProfile(player).getRank();
    }

    public static void log(String message, boolean error) {
        if (error)
            Logger.error(message);
        else
            Logger.success(message);
    }
}
