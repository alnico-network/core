package com.alniconetwork.core.spigot.staff;

import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@AllArgsConstructor
public class StaffListener implements Listener, PluginMessageListener {

    private CorePlugin plugin;

    public void onPluginMessageReceived(String channel, Player player, byte[] bytes)
    {
        if ( !channel.equalsIgnoreCase( "an:channel" ) )
        {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput( bytes );
        String subChannel = in.readUTF();

        if ( subChannel.equalsIgnoreCase( CorePlugin.getInstance().getConfig().getString("server-name" ) ))
        {
            String playername = in.readUTF();
            String action = in.readUTF();
            String servername = CorePlugin.getInstance().getConfig().getString("server-name");
            Player p = Bukkit.getPlayerExact(playername);

            if(action.equalsIgnoreCase("join")) {
                CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

                if (!p.hasPermission("core.staff")) {
                    return;
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                    Map<String, Object> messageMap = new HashMap<>();
                    messageMap.put("server", servername);
                    messageMap.put("rank", coreProfile.getRank().getName());
                    messageMap.put("player", p.getName());

                    plugin.getManagerHandler().getRedisManager().send("staff-join", messageMap);
                }, 10L);
            } else if(action.equalsIgnoreCase("leave")) {
                    OfflinePlayer f = Bukkit.getOfflinePlayer(playername);
                    String rank = "Default";
                    MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("playerdata");
                    Document document = (Document) mongoCollection.find(Filters.eq("uuid", f.getUniqueId().toString())).first();

                    if (document != null) {
                        if (this.plugin.getManagerHandler().getRankManager().getRank(document.getString("rank")) != null) {
                            rank = this.plugin.getManagerHandler().getRankManager().getRank(document.getString("rank")).getName();
                            if (!this.plugin.getManagerHandler().getRankManager().getRank(document.getString("rank")).getPermissions().contains("core.staff")) {
                                return;
                            }
                        }
                    }

                    Map<String, Object> messageMap = new HashMap<>();
                    messageMap.put("rank", rank);
                    messageMap.put("player", f.getName());

                    plugin.getManagerHandler().getRedisManager().send("staff-leave", messageMap);
            } else {
                // Switch
                CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

                if (!p.hasPermission("core.staff")) {
                    return;
                }

                Map<String, Object> messageMap = new HashMap<>();
                messageMap.put("server", action);
                messageMap.put("old", CorePlugin.getInstance().getConfig().getString("server-name"));
                messageMap.put("rank", coreProfile.getRank().getName());
                messageMap.put("player", p.getName());

                plugin.getManagerHandler().getRedisManager().send("staff-switch", messageMap);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        return;

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Player player = event.getPlayer();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (coreProfile.isStaffChat()) {
            event.setCancelled(true);

            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("server", CorePlugin.getInstance().getConfig().getString("server-name"));
            messageMap.put("rank", coreProfile.getRank().getName());
            messageMap.put("player", player.getName());
            messageMap.put("message", event.getMessage());

            plugin.getManagerHandler().getRedisManager().send("staff-chat", messageMap);
        }
    }
}
