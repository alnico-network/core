package com.alniconetwork.core.spigot.rank;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class Rank {

    private final String name;

    private String prefix = "";
    private String litePrefix = ChatColor.WHITE.toString();
    private String suffix = "";
    private int weight;
    private List<String> permissions = new ArrayList<>();
    private List<String> inheritedPermissions = new ArrayList<>();
    private List<String> townyPermissions = new ArrayList<>();
}
