package com.alniconetwork.core.spigot.punishment.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.StringUtil;
import com.alniconetwork.core.spigot.util.Strings;
import org.bson.Document;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class BlacklistCommand {

    private UUID uuid;
    private String reason;
    private String name;
    private List<String> ips;
    private Punishment punishment;
    private boolean kick;
    private boolean silent;

    private final CorePlugin plugin;

    @Command(name = "blacklist", permission = "core.command.blacklist")
    public void ban(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> [-s] [reason]");
            return;
        }
        uuid = null;
        name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        reason = "";
        silent = false;
        if (args.length() > 1) {

            reason = StringUtil.buildString(args.getArgs(), 1);

            silent = reason.contains("-s");
            reason = reason.replaceFirst("-s", "");
            reason = reason.replace("  ", " ");
            if (reason.startsWith(" ")) {
                reason = reason.replaceFirst(" ", "");
            }
        }
        if (reason.endsWith(" ")) {
            reason = StringUtil.replaceLast(reason, " ", "");
        }
        if (reason.replace(" ", "").equalsIgnoreCase("")) {
            reason = plugin.getConfig().getString("punishment.default-blacklist-reason");
        }

        ips = new ArrayList<>();
        punishment = null;
        MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("blacklists");
        CompletableFuture.runAsync(() -> {

            if (target != null) {
                CoreProfile targetProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);
                ips = targetProfile.getIps();
            } else {
                MongoCollection playerData = this.plugin.getManagerHandler().getMongoManager().getCollection("playerdata");
                Document playerDocument = (Document) playerData.find(Filters.eq("uuid", uuid.toString())).first();

                if (playerDocument == null) {
                    args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", name));
                    return;
                }
                ips = (List<String>) playerDocument.get("ips");
            }

            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {

                for (String ip : ips) {

                    Document foundDocument = (Document) mongoCursor.next();

                    punishment = this.plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);
                    if (punishment.isActive()) {
                        punishment.setActive(false);
                        punishment.setRemovedBy(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null);
                        punishment.setRemovalReason("Blacklist Override");
                        punishment.setRemovalDate(System.currentTimeMillis());
                    }
                    Document document = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);
                    document.put("ip", ip);

                    mongoCollection.deleteOne(foundDocument);
                    mongoCollection.insertOne(document);
                }
            }


            punishment = new Punishment(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null, Punishment.PunishmentType.BLACKLIST, System.currentTimeMillis(), "Forever", System.currentTimeMillis() - 1, reason, true, null, "", 0);

            for (String ip : ips) {
                Document newDocument = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);
                newDocument.put("ip", ip);

                mongoCollection.insertOne(newDocument);
            }

            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("type", "blacklist");
            messageMap.put("player", name);
            messageMap.put("reason", reason);
            messageMap.put("ips", Strings.join(ips, ":"));

            this.plugin.getManagerHandler().getRedisManager().send("punishment", messageMap);

        });

        String broadcast = (silent ? Messages.BLACKLIST_BROADCAST_SILENT : Messages.BLACKLIST_BROADCAST).getMessage()
                .replace("%player%", args.getSender().getName())
                .replace("%player1%", (target == null ? name : target.getName()))
                .replace("%reason%", reason);

        if (silent) {
            this.plugin.getServer().broadcast(broadcast, "core.silent");
            this.plugin.getServer().broadcast(broadcast, Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
        } else
            this.plugin.getServer().broadcastMessage(broadcast);
        return;
    }
}
