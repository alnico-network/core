package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class ToggleGlobalChatCommand {

    private CorePlugin plugin;

    @Command(name = "toggleglobalchat", aliases = {"tgc", "togglegc"}, inGameOnly = true)
    public void toggleGlobalChat(CommandArgs args) {
        Player player = args.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        coreProfile.setGlobalChat(!coreProfile.isGlobalChat());

        args.getSender().sendMessage(coreProfile.isGlobalChat() ? Messages.GLOBAL_CHAT_ENABLED.getMessage() : Messages.GLOBAL_CHAT_DISABLED.getMessage());
    }

}
