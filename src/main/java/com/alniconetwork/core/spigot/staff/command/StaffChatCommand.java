package com.alniconetwork.core.spigot.staff.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class StaffChatCommand {

    private CorePlugin plugin;

    @Command(name = "staffchat", aliases = {"sc"}, permission = "core.command.staffchat", inGameOnly = true)
    public void staffChat(CommandArgs args) {
        Player player = args.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (args.length() == 0) {
            coreProfile.setStaffChat(!coreProfile.isStaffChat());

            args.getSender().sendMessage(coreProfile.isStaffChat() ? Messages.STAFF_CHAT_ENABLED.getMessage() : Messages.STAFF_CHAT_DISABLED.getMessage());
            return;
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("server", CorePlugin.getInstance().getConfig().getString("server-name"));
        messageMap.put("rank", coreProfile.getRank().getName());
        messageMap.put("player", args.getPlayer().getName());
        messageMap.put("message", StringUtil.buildString(args.getArgs(), 0));

        plugin.getManagerHandler().getRedisManager().send("staff-chat", messageMap);
        return;
    }
}
