package com.alniconetwork.core.spigot.staff;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class StaffManager extends Manager {

    public StaffManager(ManagerHandler managerHandler) {
        super(managerHandler);
    }

    public void sendStaffChat(String server, Rank rank, String player, String message) {
        staffAction(staff -> staff.sendMessage(
                CC.translate(managerHandler.getPlugin().getConfig().getString("chat.staff"))
                        .replace("%server%", server)
                        .replace("%prefix%", CC.translate(rank == null || rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix()))
                        .replace("%litePrefix%", CC.translate(rank == null || rank.getLitePrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getLitePrefix()))
                        .replace("%player%", player)
                        .replace("%message%", message)));
    }

    public void sendStaffRequest(String server, Rank rank, String player, String message) {
        staffAction(staff -> staff.sendMessage(
                Messages.REQUEST.getMessage()
                        .replace("%server%", server)
                        .replace("%prefix%", CC.translate(rank == null || rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix()))
                        .replace("%litePrefix%", CC.translate(rank == null || rank.getLitePrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getLitePrefix()))
                        .replace("%player%", player)
                        .replace("%reason%", message)));
    }

    public void sendStaffReport(String server, Rank rank, String player, Rank targetRank, String target, String message) {
        staffAction(staff -> staff.sendMessage(
                Messages.REPORT.getMessage()
                        .replace("%server%", server)
                        .replace("%prefix%", CC.translate(rank == null || rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix()))
                        .replace("%litePrefix%", CC.translate(rank == null || rank.getLitePrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getLitePrefix()))
                        .replace("%player%", player)
                        .replace("%reason%", message)
                        .replace("%targetPrefix%", CC.translate(targetRank == null || targetRank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : targetRank.getPrefix()))
                        .replace("%targetLitePrefix%", CC.translate(targetRank == null || targetRank.getLitePrefix().equalsIgnoreCase("") ? CC.WHITE : targetRank.getLitePrefix()))
                        .replace("%target%", target)));
    }

    public void sendStaffStatus(String server, Rank rank, String player, StaffJoinType staffJoinType, String server1) {
        staffAction(staff -> staff.sendMessage(
                (staffJoinType == StaffJoinType.JOIN ? Messages.STAFF_JOINED : staffJoinType == StaffJoinType.QUIT ? Messages.STAFF_LEFT : Messages.STAFF_SWITCH).getMessage()
                        .replace("%server%", (server == null ? "N/A" : server))
                        .replace("%prefix%", CC.translate(rank == null || rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix()))
                        .replace("%litePrefix%", CC.translate(rank == null || rank.getLitePrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getLitePrefix()))
                        .replace("%server1%", (server1 == null ? "N/A" : server1))
                        .replace("%player%", player)));
    }

    private void staffAction(Consumer<? super Player> action) {
        managerHandler.getPlugin().getServer().getOnlinePlayers().stream().filter(p -> p.hasPermission("core.staff")).forEach(action::accept);
    }
}
