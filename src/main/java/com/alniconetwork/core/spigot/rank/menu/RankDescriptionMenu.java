package com.alniconetwork.core.spigot.rank.menu;

import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import lombok.Getter;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.ColorUtil;
import com.alniconetwork.core.spigot.util.CorrespondingWool;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

@Getter
public class RankDescriptionMenu extends Menu {

    private Rank rank;
    private int page;

    private int previousPage;
    private int maxPages;
    private int nextPage;

    public RankDescriptionMenu(Rank rank, int page) {
        super(rank.getLitePrefix() + rank.getName(), 18, true);

        this.rank = rank;
        this.page = page;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        previousPage = page - 1 <= 1 ? 1 : page - 1;

        maxPages = rank.getPermissions().size() / 9;
        if (rank.getPermissions().size() % 9 != 0) {
            maxPages += 1;
        }

        nextPage = page + 1 >= maxPages ? maxPages : page + 1;

        inventory.setItem(0,
                new ItemBuilder(Material.LEGACY_CARPET)
                        .setDurability((short) 7)
                        .setName(CC.GRAY + (previousPage == 1 ? "First Page" : "Page " + previousPage))
                        .toItemStack());

        inventory.setItem(4,
                new ItemBuilder(Material.LEGACY_WOOL)
                        .setDurability((short) CorrespondingWool
                                .getByColor(
                                        ColorUtil.getChatColor(rank.getLitePrefix())).getId())
                        .setName(rank.getLitePrefix() + rank.getName())
                        .setLore(
                                CC.GRAY + "Prefix: " + CC.RESET + rank.getPrefix(),
                                CC.GRAY + "Suffix: " + CC.RESET + rank.getSuffix(),
                                CC.GRAY + "Lite Prefix: " + CC.AQUA + ColorUtil.strip(rank.getLitePrefix()),
                                CC.GRAY + "Weight: " + CC.AQUA + rank.getWeight(),
                                " ",
                                CC.GRAY + "Click for management options."
                        )
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.LEGACY_CARPET)
                        .setDurability((short) 7)
                        .setName(CC.GRAY + (nextPage == maxPages ? "Last Page" : "Page " + nextPage))
                        .toItemStack());

        int start = page * 9 - 9;
        int end = start + 9;

        int slot = 9;

        for (int i = start; i < end; i++) {

            if (i < rank.getPermissions().size()) {

                String permission = rank.getPermissions().get(i);

                inventory.setItem(slot,
                        new ItemBuilder(Material.PAPER)
                                .setName(rank.getLitePrefix() + permission)
                                .setLore(CC.GRAY + "Click here to remove this permission.")
                                .toItemStack());
            } else {
                inventory.setItem(slot,
                        new ItemBuilder(Material.LEGACY_STAINED_GLASS_PANE)
                                .setDurability((short) 7)
                                .setName(" ")
                                .toItemStack());
            }
            slot += 1;
        }
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        if (currentItem.getType() == Material.PAPER) {

            String permission = rank.getPermissions().get(page * 9 - 18 + slot);

            RankRemovePermissionMenu rankRemovePermissionMenu = new RankRemovePermissionMenu(rank, permission);
            rankRemovePermissionMenu.open(player);
        } else {
            switch (slot) {
                case 0: {
                    if (previousPage != page) {
                        RankDescriptionMenu rankDescriptionMenu = new RankDescriptionMenu(rank, previousPage);

                        rankDescriptionMenu.open(player);
                    }
                    break;
                }
                case 4: {
                    RankManagementMenu rankManagementMenu = new RankManagementMenu(rank);
                    rankManagementMenu.open(player);
                    break;
                }
                case 8: {
                    if (nextPage != page) {
                        RankDescriptionMenu rankDescriptionMenu = new RankDescriptionMenu(rank, nextPage);

                        rankDescriptionMenu.open(player);
                    }
                    break;
                }
            }
        }
        menuEvent.setCancelled(true);
    }
}
