package com.alniconetwork.core.spigot.ipwl;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.Logger;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.net.InetAddress;

public class IPWLListener implements Listener {

    private CorePlugin plugin;

    public IPWLListener(CorePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerLogin(final PlayerLoginEvent ev) {
        final InetAddress addr = ev.getRealAddress();
        final Player player = ev.getPlayer();
        if(plugin.getConfig().getBoolean("bungee.enabled")) {
            if(plugin.getConfig().getString("bungee.ip").contains(addr.toString())) {
                Logger.error(player.getName() + " just tried to use an unauthorized bungee ip address of " + addr.toString());
                player.kickPlayer(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("bungee.msg")));
            } else if(plugin.getConfig().getBoolean("bungee.debug")) {
                Logger.success(player.getName() + " logged in from an authorized bungee ip of " + addr.toString());
        }
        }
    }

}
