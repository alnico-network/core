package com.alniconetwork.core.spigot.player;

import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import me.clip.placeholderapi.PlaceholderAPI;

import java.util.concurrent.TimeUnit;

@AllArgsConstructor
public class PlayerListener implements Listener {

    private CorePlugin plugin;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (event.isCancelled()) return;

        if (this.plugin.getManagerHandler().getPlayerManager().isChatMuted() && !player.hasPermission("core.staff")) {
            event.setCancelled(true);
            player.sendMessage(Messages.CHAT_IS_MUTED.getMessage());
            return;
        }
        if (System.currentTimeMillis() <= coreProfile.getLastChat() + 1000 * this.plugin.getManagerHandler().getPlayerManager().getChatDelay() && !player.hasPermission("core.chat.bypass")) {
            event.setCancelled(true);

            long difference = TimeUnit.MILLISECONDS.toSeconds(coreProfile.getLastChat() + (this.plugin.getManagerHandler().getPlayerManager().getChatDelay() * 1000) - System.currentTimeMillis());

            player.sendMessage(Messages.MUST_WAIT_TO_CHAT.getMessage().replace("%time%", String.valueOf(difference)));
            return;
        }

        this.plugin.getServer().getOnlinePlayers().forEach(p -> {
            CoreProfile coreProfiles = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

            if (!player.hasPermission("core.staff") && !coreProfiles.isGlobalChat()) event.getRecipients().remove(p);
        });

        Rank rank = coreProfile.getRank();

        String chat = CC.translate(this.plugin.getConfig().getString("chat.format" + (rank == null || rank.getSuffix().equalsIgnoreCase("") ? "" : "-suffix")))
                .replace("%prefix%", rank == null || rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix())  //rank.getPrefix().equalsIgnoreCase("") ? CC.WHITE : rank.getPrefix()
                .replace("%player%", player.getName())
                .replace("%suffix%", rank == null || rank.getSuffix().equalsIgnoreCase("") ? CC.WHITE : rank.getSuffix())
                .replace("%message%", event.getMessage().replace("%", "%%"));

        chat = PlaceholderAPI.setPlaceholders(player, chat);

        event.setFormat(chat);

        coreProfile.setLastChat(System.currentTimeMillis());
        return;
    }
}