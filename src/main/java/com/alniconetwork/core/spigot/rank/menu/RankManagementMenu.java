package com.alniconetwork.core.spigot.rank.menu;

import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class RankManagementMenu extends Menu {

    private Rank rank;
    private boolean settingPrefix;
    private boolean settingLitePrefix;
    private boolean settingSuffix;
    private boolean settingWeight;

    public RankManagementMenu(Rank rank) {
        super("Manage " + rank.getName(), 9, false);

        this.rank = rank;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        inventory.setItem(1,
                new ItemBuilder(Material.DIAMOND)
                        .setName(CC.PINK + "Set Prefix")
                        .setLore("",
                                CC.BLUE + "Set the prefix of rank " + rank.getName())
                        .toItemStack());

        inventory.setItem(3,
                new ItemBuilder(Material.LEGACY_SKULL_ITEM)
                        .setName(CC.PINK + "Set Lite Prefix")
                        .setLore("",
                                CC.BLUE + "Set the lite prefix of rank " + rank.getName())
                        .toItemStack());

        inventory.setItem(5,
                new ItemBuilder(Material.SIGN)
                        .setName(CC.PINK + "Set Suffix")
                        .setLore("",
                                CC.BLUE + "Set the suffix of rank " + rank.getName())
                        .toItemStack());

        inventory.setItem(7,
                new ItemBuilder(Material.ENCHANTED_BOOK)
                        .setName(CC.PINK + "Set Weight")
                        .setLore("",
                                CC.BLUE + "Set the weight of rank " + rank.getName())
                        .toItemStack());
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        switch (slot) {
            case 1: {
                settingPrefix = true;
                player.closeInventory();

                player.sendMessage(Messages.RANK_SETTING_PREFIX.getMessage()
                        .replace("%rank%", rank.getName()));
                break;
            }
            case 3: {
                settingLitePrefix = true;
                player.closeInventory();

                player.sendMessage(Messages.RANK_SETTING_LITE_PREFIX.getMessage()
                        .replace("%rank%", rank.getName()));
                break;
            }
            case 5: {
                settingSuffix = true;
                player.closeInventory();

                player.sendMessage(Messages.RANK_SETTING_SUFFIX.getMessage()
                        .replace("%rank%", rank.getName()));
                break;
            }
            case 7: {
                settingWeight = true;
                player.closeInventory();

                player.sendMessage(Messages.RANK_SETTING_WEIGHT.getMessage()
                        .replace("%rank%", rank.getName()));
                break;
            }
        }
    }

    @EventHandler
    public void onChat(PlayerChatEvent event) {
        Player player = event.getPlayer();

        if (player != this.getPlayer()) return;

        if (settingPrefix) {
            event.setCancelled(true);

            player.performCommand("rank setprefix " + rank.getName() + " " + event.getMessage());
            settingPrefix = false;
        } else if (settingLitePrefix) {
            event.setCancelled(true);

            player.performCommand("rank setliteprefix " + rank.getName() + " " + event.getMessage());
            settingLitePrefix = false;
        } else if (settingSuffix) {
            event.setCancelled(true);

            player.performCommand("rank setsuffix " + rank.getName() + " " + event.getMessage());
            settingSuffix = false;
        } else if (settingWeight) {
            event.setCancelled(true);

            player.performCommand("rank setweight " + rank.getName() + " " + event.getMessage());
            settingWeight = false;
        }

    }
}
