package com.alniconetwork.core.spigot.grant;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
public class GrantListener implements Listener {

    private CorePlugin plugin;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);
        CompletableFuture.runAsync(() ->
                fetchGrants(player, coreProfile));
    }

    public static void fetchGrants(Player player, CoreProfile coreProfile) {
        MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("grants");
        MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).iterator();

        while (mongoCursor.hasNext()) {
            Document foundDocument = (Document) mongoCursor.next();

            Grant grant = CorePlugin.getInstance().getManagerHandler().getGrantManager().fromDocument(foundDocument);
            coreProfile.getGrantList().add(grant);
        }
    }
}
