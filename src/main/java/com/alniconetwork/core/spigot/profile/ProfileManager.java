package com.alniconetwork.core.spigot.profile;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProfileManager extends Manager {

    private Map<UUID, CoreProfile> coreProfileMap;

    public ProfileManager(ManagerHandler managerHandler) {
        super(managerHandler);

        coreProfileMap = new HashMap<>();
    }

    public void addPlayer(Player player) {
        coreProfileMap.put(player.getUniqueId(), new CoreProfile());
    }

    public void removePlayer(Player player) {
        coreProfileMap.remove(player.getUniqueId());
    }

    public boolean hasProfile(Player player) {
        return coreProfileMap.containsKey(player.getUniqueId());
    }

    public CoreProfile getProfile(Player player) {
        return coreProfileMap.get(player.getUniqueId());
    }
}