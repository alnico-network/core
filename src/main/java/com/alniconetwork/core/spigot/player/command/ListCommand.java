package com.alniconetwork.core.spigot.player.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Strings;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public class ListCommand {

    private CorePlugin plugin;

    @Command(name = "list", aliases = {"players"})
    public void list(CommandArgs args) {
        List<Rank> sortedRanks = new ArrayList<>();
        sortedRanks = new ArrayList<>(this.plugin.getManagerHandler().getRankManager().getRankList());
        sortedRanks.sort(Comparator.comparing(Rank::getWeight));

        List<String> ranks = new ArrayList<>();
        List<String> players = new ArrayList<>();

        sortedRanks.forEach(r -> {
            ranks.add(CC.translate(r.getLitePrefix() + r.getName()));

            this.plugin.getServer().getOnlinePlayers().forEach(p -> {
                CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

                if (coreProfile.getRank() == r) {
                    players.add(CC.translate(r.getLitePrefix() + p.getName()));
                }
            });
        });

        args.getSender().sendMessage(Messages.LIST.getMessage()
                .replace("%ranks%", Strings.join(ranks, CC.GRAY + ", "))
                .replace("%online%", String.valueOf(this.plugin.getServer().getOnlinePlayers().size()))
                .replace("%max%", String.valueOf(this.plugin.getServer().getMaxPlayers()))
                .replace("%players%", Strings.join(players, CC.GRAY + ", ")));
    }
}
