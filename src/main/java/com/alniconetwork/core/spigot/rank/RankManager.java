package com.alniconetwork.core.spigot.rank;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.Getter;
import lombok.Setter;
import com.alniconetwork.core.spigot.util.Logger;
import org.bson.Document;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

@Getter
public class RankManager extends Manager {

    private List<Rank> rankList;

    @Setter
    private Rank defaultRank;

    public RankManager(ManagerHandler managerHandler) {
        super(managerHandler);

        rankList = new ArrayList<>();
        fetch();
    }

    private void fetch() {
        MongoCollection mongoCollection = this.managerHandler.getMongoManager().getMongoDatabase().getCollection("ranks");
        MongoCursor mongoCursor = mongoCollection.find().iterator();
        while (mongoCursor.hasNext()) {
            Document document = (Document) mongoCursor.next();

            Rank rank = new Rank(document.getString("name"));
            rank.setPrefix(document.getString("prefix"));
            rank.setLitePrefix(document.getString("liteprefix"));
            rank.setSuffix(document.getString("suffix"));
            rank.setWeight(document.getInteger("weight"));
            rank.setPermissions((List<String>) document.get("permissions"));
            rank.setTownyPermissions((List<String>) document.get("townyperms"));

            rankList.add(rank);
            Logger.success("Loaded rank " + rank.getName() + ".");
        }
        if (getRank(managerHandler.getPlugin().getConfig().getString("default-rank")) != null)
            defaultRank = getRank(managerHandler.getPlugin().getConfig().getString("default-rank"));

        if (!managerHandler.getPlugin().getConfig().getBoolean("auto-inherit")) return;

        for (Rank rank : rankList) {
            for (Rank rankBelow : rankList) {

                if (rankBelow.getWeight() < rank.getWeight()) {
                    for (String permission : rankBelow.getPermissions()) {
                        if (!rank.getPermissions().contains(permission) && !rank.getInheritedPermissions().contains(permission)) {

                            rank.getInheritedPermissions().add(permission);
                        }
                    }
                }
            }
        }
    }

    public void addRank(Rank rank) {
        rankList.add(rank);
    }

    public void removeRank(Rank rank) {
        rankList.remove(rank);
    }

    public Rank getRank(String name) {
        return rankList.stream().filter(r -> r.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public void updatePermissions(Player player) {
        CoreProfile coreProfile = this.managerHandler.getProfileManager().getProfile(player);

        if (coreProfile.getRank() == null) return;

        unsetPermissions(player);

        PermissionAttachment permissionAttachment = coreProfile.getPermissionAttachment();
        coreProfile.getRank().getPermissions().forEach(p -> permissionAttachment.setPermission(p, true));
        coreProfile.getRank().getInheritedPermissions().forEach(p -> permissionAttachment.setPermission(p, true));
        if(managerHandler.getPlugin().getConfig().getString("server-name").equalsIgnoreCase("towny")) {
            coreProfile.getRank().getTownyPermissions().forEach(p -> permissionAttachment.setPermission(p, true));
        }
        if (!player.isOp()) {
            for (String permission : managerHandler.getPlugin().getConfig().getStringList("disallowed-permissions")) {
                permissionAttachment.setPermission(permission, false);
            }
        }
    }

    public void unsetPermissions(Player player) {
        CoreProfile coreProfile = this.managerHandler.getProfileManager().getProfile(player);

        if (coreProfile.getPermissionAttachment() == null) {
            coreProfile.setPermissionAttachment(player.addAttachment(managerHandler.getPlugin()));
        }
        PermissionAttachment permissionAttachment = coreProfile.getPermissionAttachment();
        permissionAttachment.getPermissions().keySet().forEach(p -> permissionAttachment.setPermission(p, false));
    }

    public void save() {
        MongoCollection mongoCollection = this.managerHandler.getMongoManager().getMongoDatabase().getCollection("ranks");

        rankList.forEach(rank -> {

            Document document = (Document) mongoCollection.find(Filters.eq("name", rank.getName())).first();

            if (document != null) mongoCollection.deleteOne(document);

            HashMap<String, Object> documentMap = new HashMap<>();

            documentMap.put("name", rank.getName());
            documentMap.put("prefix", rank.getPrefix());
            documentMap.put("liteprefix", rank.getLitePrefix());
            documentMap.put("suffix", rank.getSuffix());
            documentMap.put("weight", rank.getWeight());
            documentMap.put("permissions", rank.getPermissions());
            documentMap.put("townyperms", rank.getTownyPermissions());

            mongoCollection.insertOne(new Document(documentMap));
        });

        MongoCursor mongoCursor = mongoCollection.find().iterator();

        while (mongoCursor.hasNext()) {
            Document document = (Document) mongoCursor.next();

            String name = document.getString("name");
            if (getRank(name) == null) mongoCollection.deleteOne(document);
        }

        if (defaultRank != null) {
            managerHandler.getPlugin().getConfig().set("default-rank", defaultRank.getName());
            managerHandler.getPlugin().saveConfig();
        }
    }
}
