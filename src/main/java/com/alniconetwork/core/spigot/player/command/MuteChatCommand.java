package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;

@AllArgsConstructor
public class MuteChatCommand {

    private CorePlugin plugin;

    @Command(name = "mutechat", aliases = {"mc"}, permission = "core.command.mutechat")
    public void muteChat(CommandArgs args) {
        this.plugin.getManagerHandler().getPlayerManager().setChatMuted(!this.plugin.getManagerHandler().getPlayerManager().isChatMuted());

        this.plugin.getServer().broadcastMessage(this.plugin.getManagerHandler().getPlayerManager().isChatMuted() ? Messages.PUBLIC_CHAT_MUTED.getMessage() : Messages.PUBLIC_CHAT_ENABLED.getMessage());
        return;
    }
}
