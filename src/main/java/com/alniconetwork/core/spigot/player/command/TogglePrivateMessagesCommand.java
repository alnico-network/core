package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class TogglePrivateMessagesCommand {

    private CorePlugin plugin;

    @Command(name = "toggleprivatemessages", aliases = {"tpm", "tpms", "togglepms"}, inGameOnly = true)
    public void toggleGlobalChat(CommandArgs args) {
        Player player = args.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        coreProfile.setPms(!coreProfile.isPms());

        args.getSender().sendMessage(coreProfile.isPms() ? Messages.PRIVATE_MESSAGES_ENABLED.getMessage() : Messages.PRIVATE_MESSAGES_DISABLED.getMessage());
        return;
    }

}
