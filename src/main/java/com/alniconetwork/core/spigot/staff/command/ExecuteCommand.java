package com.alniconetwork.core.spigot.staff.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;

import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
public class ExecuteCommand {

    private CorePlugin plugin;

    @Command(name = "execute", permission = "core.command.execute")
    public void execute(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <server> <command>");
            return;
        }
        String server = args.getArgs(0);
        String command = StringUtil.buildString(args.getArgs(), 1);

        String message = "execute//" + server + "//" + command;
        CompletableFuture.runAsync(() ->
                this.plugin.getManagerHandler().getRedisManager().getJedisPool().getResource().publish("mCore", message));

        args.getSender().sendMessage(Messages.SENT_COMMAND.getMessage()
                .replace("%server%", server)
                .replace("%command%", command));
        return;
    }
}
