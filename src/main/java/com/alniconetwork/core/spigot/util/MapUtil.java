package com.alniconetwork.core.spigot.util;

import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class MapUtil {

    public static Map<String, Object> cloneMap(Document document) {
        Map<String, Object> documentMap = new HashMap<>();
        document.keySet().forEach(s -> documentMap.put(s, document.get(s)));

        return documentMap;
    }
}
