package com.alniconetwork.core.spigot.redis;

import com.google.gson.reflect.TypeToken;
import com.alniconetwork.core.spigot.CorePlugin;

import java.lang.reflect.Type;
import java.util.Map;

public class RedisMessage {

    public static Map<String, Object> deserialize(String string) {
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return CorePlugin.getGson().fromJson(string, type);
    }

    public static String serialize(Map<String, Object> map) {
        return CorePlugin.getGson().toJson(map);
    }

}