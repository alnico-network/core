package com.alniconetwork.core.spigot.staff.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class FreezeCommand {

    private CorePlugin plugin;

    @Command(name = "freeze", aliases = {"ss"}, permission = "core.command.freeze")
    public void freezeCommand(CommandArgs args) {
        if (args.length() != 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player>");
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        Player player = args.getPlayer();
        if (player == target) {
            args.getSender().sendMessage(Messages.CANT_FREEZE_YOURSELF.getMessage());
            return;
        }
        CoreProfile targetProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);
        targetProfile.setFrozen(!targetProfile.isFrozen());

        args.getSender().sendMessage((targetProfile.isFrozen() ? Messages.FROZE_PLAYER : Messages.UNFROZE_PLAYER).getMessage().replace("%player%", target.getName()));

        if (!targetProfile.isFrozen()) target.sendMessage(Messages.UNFROZEN.getMessage());
        return;
    }
}
