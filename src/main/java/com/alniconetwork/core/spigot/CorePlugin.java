package com.alniconetwork.core.spigot;

import com.alniconetwork.core.spigot.command.CommandFramework;
import com.alniconetwork.core.spigot.config.ReloadConfigCommand;
import com.alniconetwork.core.spigot.grant.GrantListener;
import com.alniconetwork.core.spigot.ipwl.IPWLListener;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import com.alniconetwork.core.spigot.player.PlayerListener;
import com.alniconetwork.core.spigot.player.command.*;
import com.alniconetwork.core.spigot.profile.ProfileListener;
import com.alniconetwork.core.spigot.profile.ProfileTask;
import com.alniconetwork.core.spigot.punishment.PunishmentListener;
import com.alniconetwork.core.spigot.punishment.command.*;
import com.alniconetwork.core.spigot.rank.RankTask;
import com.alniconetwork.core.spigot.rank.command.RankCommand;
import com.alniconetwork.core.spigot.staff.StaffListener;
import com.alniconetwork.core.spigot.staff.command.AltsCommand;
import com.alniconetwork.core.spigot.staff.command.ExecuteCommand;
import com.alniconetwork.core.spigot.staff.command.FreezeCommand;
import com.alniconetwork.core.spigot.staff.command.StaffChatCommand;
import com.alniconetwork.core.spigot.util.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import com.alniconetwork.core.spigot.grant.command.GrantCommand;
import com.alniconetwork.core.spigot.grant.command.GrantHistoryCommand;
import com.alniconetwork.core.spigot.util.BroadcasterTask;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

@Getter
public class CorePlugin extends JavaPlugin {

    @Getter
    private static CorePlugin instance;
    @Getter
    private static Gson gson;

    public static String VERSION = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];

    private ManagerHandler managerHandler;

    @Override
    public void onEnable() {
        instance = this;
        gson = new GsonBuilder().setPrettyPrinting().create();
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        this.saveResource("config.yml", false);
        managerHandler = new ManagerHandler(this);
        managerHandler.register();
        getServer().getMessenger().registerIncomingPluginChannel( this, "an:channel", new StaffListener(this));
        registerCommands(
                new RankCommand(this),
                new GrantCommand(this),
                new GrantHistoryCommand(this),
                new BanCommand(this),
                new UnbanCommand(this),
                new PunishmentHistoryCommand(this),
                new MuteCommand(this),
                new UnmuteCommand(this),
                new KickCommand(this),
                new BroadcastCommand(this),
                new ClearChatCommand(this),
                new FeedCommand(this),
                new GameModeCommand(this),
                new HealCommand(this),
                new MoreCommand(),
                new MuteChatCommand(this),
                new RenameCommand(),
                new SlowChatCommand(this),
                new ListCommand(this),
                new MessageCommand(this),
                new PingCommand(this),
                new ReplyCommand(this),
                new ReportCommand(this),
                new RequestCommand(this),
                new ToggleGlobalChatCommand(this),
                new TogglePrivateMessagesCommand(this),
                new FreezeCommand(this),
                new StaffChatCommand(this),
                new ExecuteCommand(this),
                new BlacklistCommand(this),
                new UnblacklistCommand(this),
                new AltsCommand(this),
                new ReloadConfigCommand(this)
        );

        registerListeners(
                new PunishmentListener(),
                new PlayerListener(this),
                new ProfileListener(this),
                new StaffListener(this),
                new GrantListener(this),
                new IPWLListener(this)
        );

        if (getConfig().getBoolean("auto-broadcaster.enabled"))
            new BroadcasterTask(this).runTaskTimerAsynchronously(this, 100L, 20 * (getConfig().getInt("auto-broadcaster.frequency")));

        new ProfileTask(this).runTaskTimerAsynchronously(this, 0L, 20L);
        new RankTask(this).runTaskTimerAsynchronously(this, 0L, 0L);
    }

    @Override
    public void onDisable() {
        this.getServer().getOnlinePlayers().stream().forEach(p -> p.kickPlayer("Server restarting."));
        this.managerHandler.getRankManager().save();
     //   this.managerHandler.getRedisManager().getJedisPool().close();
    }

    public void registerCommands(Object... objects) {
        CommandFramework commandFramework = new CommandFramework(this);
        Arrays.stream(objects).forEach(o -> commandFramework.registerCommands(o));
    }

    private void registerListeners(Listener... listeners) {
        Arrays.stream(listeners).forEach(l -> getServer().getPluginManager().registerEvents(l, this));
    }

}
