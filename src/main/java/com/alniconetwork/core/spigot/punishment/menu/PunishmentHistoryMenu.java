package com.alniconetwork.core.spigot.punishment.menu;

import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PunishmentHistoryMenu extends Menu {

    private String name;
    private UUID uuid;
    private boolean online;
    private Punishment.PunishmentFilter punishmentFilter;
    private int page;

    private int previousPage;
    private int maxPages;
    private int nextPage;

    private List<Punishment> punishmentList = new ArrayList<>();
    private List<Punishment> allPunishments = new ArrayList<>();

    public PunishmentHistoryMenu(String name, UUID uuid, boolean online, Punishment.PunishmentFilter punishmentFilter, int page) {
        super("Punishments of " + name, 18, true);

        this.name = name;
        this.uuid = uuid;
        this.online = online;
        this.punishmentFilter = punishmentFilter;
        this.page = page;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        punishmentList.clear();
        allPunishments.clear();

        switch (punishmentFilter) {
            case NONE: {
                getBans(true);
                getBlacklists(true);
                getMutes(true);
                getKicks(true);
                break;
            }
            case BANS: {
                getBans(true);
                getBlacklists(false);
                getMutes(false);
                getKicks(false);
                break;
            }
            case BLACKLISTS: {
                getBans(false);
                getBlacklists(true);
                getMutes(false);
                getKicks(false);
                break;
            }
            case MUTES: {
                getBans(false);
                getBlacklists(false);
                getMutes(true);
                getKicks(false);
                break;
            }
            case KICKS: {
                getBans(false);
                getBlacklists(false);
                getMutes(false);
                getKicks(true);
                break;
            }
        }
        maxPages = punishmentList.size() / 9;
        if (punishmentList.size() % 9 != 0) {
            maxPages += 1;
        }

        previousPage = page - 1 <= 1 ? 1 : page - 1;

        nextPage = page + 1 >= maxPages ? maxPages : page + 1;

        inventory.setItem(0,
                new ItemBuilder(Material.LEGACY_CARPET)
                        .setDurability((short) 7)
                        .setName(CC.GRAY + (previousPage == 1 ? "First Page" : "Page " + previousPage))
                        .toItemStack());

        inventory.setItem(4,
                new ItemBuilder(Material.REDSTONE)
                        .setName(CC.AQUA + "Punishment Type")
                        .setLore(
                                (punishmentFilter == Punishment.PunishmentFilter.NONE ? CC.GREEN : CC.GRAY) + " All",
                                (punishmentFilter == Punishment.PunishmentFilter.BANS ? CC.GREEN : CC.GRAY) + " Bans",
                                (punishmentFilter == Punishment.PunishmentFilter.BLACKLISTS ? CC.GREEN : CC.GRAY) + " Blacklists",
                                (punishmentFilter == Punishment.PunishmentFilter.MUTES ? CC.GREEN : CC.GRAY) + " Mutes",
                                (punishmentFilter == Punishment.PunishmentFilter.KICKS ? CC.GREEN : CC.GRAY) + " Kicks")
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.LEGACY_CARPET)
                        .setDurability((short) 7)
                        .setName(CC.GRAY + (nextPage == maxPages ? "Last Page" : "Page " + nextPage))
                        .toItemStack());

        int start = page * 9 - 9;
        int end = start + 9;

        int slot = 9;

        for (int i = start; i < end; i++) {
            if (i < punishmentList.size() && punishmentList.size() > 0) {
                Punishment punishment = punishmentList.get(i);

                String punishedBy = punishment.getAddedBy() == null ? "CONSOLE" : Bukkit.getOfflinePlayer(punishment.getAddedBy()).getName();

                boolean perm = punishment.getExpire() < punishment.getDate();
                boolean expired = !punishment.isActive();

                boolean wasrevoked = punishment.getRemovedBy() == null ? false : true;
                if(wasrevoked) {
                    inventory.setItem(slot,
                            new ItemBuilder(Material.LEGACY_INK_SACK)
                                    .setDurability((short) (punishment.isActive() && (punishment.getDate() > punishment.getExpire() || punishment.getExpire() > System.currentTimeMillis()) ? 10 : 8))
                                    .setName(CC.AQUA + "Punishment #" + (allPunishments.indexOf(punishment) + 1))
                                    .setLore(
                                            CC.GRAY + CC.STRIKE + "---------------------",
                                            CC.GRAY + "Type: " + CC.AQUA + punishment.getPunishmentType(),
                                            CC.GRAY + "Reason: " + CC.AQUA + punishment.getReason(),
                                            CC.GRAY + "Added: " + CC.AQUA + TimeUtil.formatTimeMillis(System.currentTimeMillis() - punishment.getDate(), true) + " ago",
                                            CC.GRAY + "Added by: " + CC.AQUA + punishedBy,
                                            CC.GRAY + "Duration: " + CC.AQUA + punishment.getTime(),
                                            CC.GRAY + CC.STRIKE + "---------------------",
                                            CC.GRAY + "Removed on: " + CC.AQUA + TimeUtil.formatTimeMillis(System.currentTimeMillis() - punishment.getRemovalDate(), true),
                                            CC.GRAY + "Removed by: " + CC.AQUA + (punishment.getRemovedBy() == null ? "CONSOLE" : Bukkit.getOfflinePlayer(punishment.getRemovedBy()).getName()),
                                            CC.GRAY + "Reason: " + CC.AQUA + punishment.getRemovalReason(),
                                            CC.GRAY + CC.STRIKE + "---------------------"
                                    )
                                    .toItemStack());
                } else {
                    inventory.setItem(slot,
                            new ItemBuilder(Material.LEGACY_INK_SACK)
                                    .setDurability((short) (punishment.isActive() && (punishment.getDate() > punishment.getExpire() || punishment.getExpire() > System.currentTimeMillis()) ? 10 : 8))
                                    .setName(CC.AQUA + "Punishment #" + (allPunishments.indexOf(punishment) + 1))
                                    .setLore(
                                            CC.GRAY + CC.STRIKE + "---------------------",
                                            CC.GRAY + "Type: " + CC.AQUA + punishment.getPunishmentType(),
                                            CC.GRAY + "Reason: " + CC.AQUA + punishment.getReason(),
                                            CC.GRAY + "Added: " + CC.AQUA + TimeUtil.formatTimeMillis(System.currentTimeMillis() - punishment.getDate(), true) + " ago",
                                            CC.GRAY + "Added by: " + CC.AQUA + punishedBy,
                                            CC.GRAY + "Duration: " + CC.AQUA + punishment.getTime(),
                                            CC.GRAY + "Expire" + (expired ? "d" : "s") + ": " + CC.AQUA + (punishment.getPunishmentType() == Punishment.PunishmentType.KICK ? "N/A" : (expired ? TimeUtil.formatTimeMillis(System.currentTimeMillis() - punishment.getRemovalDate(), true) + " ago" : perm ? "Never" : TimeUtil.formatTimeMillis(punishment.getExpire() - System.currentTimeMillis(), true))),
                                            CC.GRAY + CC.STRIKE + "---------------------"
                                    )
                                    .toItemStack());
                }

            } else {
                inventory.setItem(slot,
                        new ItemBuilder(Material.LEGACY_STAINED_GLASS_PANE)
                                .setDurability((short) 7)
                                .setName(" ")
                                .toItemStack());
            }
            slot += 1;
        }
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        if (currentItem.getType() == Material.PAPER) {


        } else {
            switch (slot) {
                case 0: {
                    if (previousPage != page) {
                        Player target = Bukkit.getPlayer(uuid);
                        PunishmentHistoryMenu punishmentHistoryMenu = new PunishmentHistoryMenu(name, uuid, target != null, punishmentFilter, previousPage);

                        punishmentHistoryMenu.open(player);
                    }
                    break;
                }
                case 4: {
                    switch (punishmentFilter) {
                        case NONE: {
                            punishmentFilter = Punishment.PunishmentFilter.BANS;
                            break;
                        }
                        case BANS: {
                            punishmentFilter = Punishment.PunishmentFilter.BLACKLISTS;
                            break;
                        }
                        case BLACKLISTS: {
                            punishmentFilter = Punishment.PunishmentFilter.MUTES;
                            break;
                        }
                        case MUTES: {
                            punishmentFilter = Punishment.PunishmentFilter.KICKS;
                            break;
                        }
                        case KICKS: {
                            punishmentFilter = Punishment.PunishmentFilter.NONE;
                            break;
                        }
                    }
                    page = 1;
                    build(); // Force update and avoid flicker
                    break;
                }
                case 8: {
                    if (nextPage != page) {
                        Player target = Bukkit.getPlayer(uuid);
                        PunishmentHistoryMenu punishmentHistoryMenu = new PunishmentHistoryMenu(name, uuid, target != null, punishmentFilter, nextPage);

                        punishmentHistoryMenu.open(player);
                    }
                    break;
                }
            }
        }
        menuEvent.setCancelled(true);
    }

    private void getBans(boolean active) {
        if (online) {

            Player target = Bukkit.getPlayer(uuid);
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);

            coreProfile.getPunishmentList().stream().filter(p -> p.getPunishmentType() == Punishment.PunishmentType.BAN).forEach(p -> {
                if (active) punishmentList.add(p);
                allPunishments.add(p);
            });
        } else {
            MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("bans");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();

                Punishment punishment = CorePlugin.getInstance().getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

                if (active) punishmentList.add(punishment);
                allPunishments.add(punishment);
            }
        }
    }

    private void getBlacklists(boolean active) {
        MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("blacklists");

        List<String> ips = new ArrayList<>();
        Player target = Bukkit.getPlayer(uuid);
        if (target != null) {
            CoreProfile targetProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);
            ips = targetProfile.getIps();
        } else {
            MongoCollection playerData = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("playerdata");
            Document playerDocument = (Document) playerData.find(Filters.eq("uuid", uuid.toString())).first();
            if(playerDocument != null) {
                ips = (List<String>) playerDocument.get("ips");
            } else {
                ips = null;
            }
        }

        if (ips != null) {
            for (String ip : ips) {
                MongoCursor mongoCursor = mongoCollection.find(Filters.eq("ip", ip)).iterator();

                while (mongoCursor.hasNext()) {
                    Document foundDocument = (Document) mongoCursor.next();
                    Punishment punishment = CorePlugin.getInstance().getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

                    if (active) punishmentList.add(punishment);
                    allPunishments.add(punishment);
                }
            }
        }

    }

    private void getMutes(boolean active) {
        if (online) {

            Player target = Bukkit.getPlayer(uuid);
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);

            coreProfile.getPunishmentList().stream().filter(p -> p.getPunishmentType() == Punishment.PunishmentType.MUTE).forEach(p -> {
                if (active) punishmentList.add(p);
                allPunishments.add(p);
            });
        } else {
            MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("mutes");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();

                Punishment punishment = CorePlugin.getInstance().getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

                if (active) punishmentList.add(punishment);
                allPunishments.add(punishment);
            }
        }
    }

    private void getKicks(boolean active) {
        if (online) {

            Player target = Bukkit.getPlayer(uuid);
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);

            coreProfile.getPunishmentList().stream().filter(p -> p.getPunishmentType() == Punishment.PunishmentType.KICK).forEach(p -> {
                if (active) punishmentList.add(p);
                allPunishments.add(p);
            });
        } else {
            MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("kicks");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();

                Punishment punishment = CorePlugin.getInstance().getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

                if (active) punishmentList.add(punishment);
                allPunishments.add(punishment);
            }
        }
    }
}
