package com.alniconetwork.core.spigot.profile;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.scheduler.BukkitRunnable;

@AllArgsConstructor
public class ProfileTask extends BukkitRunnable {

    private CorePlugin plugin;

    public void run() {
        this.plugin.getServer().getOnlinePlayers().forEach(p -> {
            CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

            if (coreProfile.isFrozen()) p.sendMessage(Messages.FROZEN.getMessage());
        });
    }

}