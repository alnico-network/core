package com.alniconetwork.core.spigot.grant.command;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.grant.menu.GrantHistoryMenu;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

@AllArgsConstructor
public class GrantHistoryCommand {

    private CorePlugin plugin;

    @Command(name = "granthistory", aliases = {"grants"}, permission = "core.command.granthistory", inGameOnly = true)
    public void grantHistory(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player>");
            return;
        }
        UUID uuid = null;
        String name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        Player player = args.getPlayer();

        GrantHistoryMenu grantHistoryMenu = new GrantHistoryMenu(name, uuid, target != null, 1);
        grantHistoryMenu.open(player);
        return;
    }
}
