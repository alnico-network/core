package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.reflection.BukkitReflection;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class PingCommand {

    private CorePlugin plugin;

    @Command(name = "ping", inGameOnly = true)
    public void ping(CommandArgs args) {
        if (args.length() == 0) {
            args.getPlayer().performCommand("ping " + args.getSender().getName());
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        args.getSender().sendMessage(Messages.PING.getMessage()
                .replace("%player%", target.getName())
                .replace("%ping%", String.valueOf(BukkitReflection.getPing(target))));
        return;
    }
}
