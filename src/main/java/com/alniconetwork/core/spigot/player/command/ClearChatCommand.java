package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;

@AllArgsConstructor
public class ClearChatCommand {

    private CorePlugin plugin;

    @Command(name = "clearchat", aliases = {"chatclear", "cc"}, permission = "core.command.clearchat")
    public void clearChat(CommandArgs args) {
        this.plugin.getServer().getOnlinePlayers().stream().filter(p -> !p.hasPermission("core.staff")).forEach(p -> {
            for (int i = 0; i <= 1000; i++) p.sendMessage(" ");
        });
        this.plugin.getServer().broadcastMessage(Messages.CHAT_CLEARED.getMessage().replace("%player%", args.getSender().getName()));
        return;
    }
}
