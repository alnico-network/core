package com.alniconetwork.core.spigot.grant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class Grant {

    private UUID addedBy;
    private long date;
    private String rank;
    private String time;
    private long expire;
    private String reason;

    @Setter
    private boolean active;
}
