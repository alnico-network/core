package com.alniconetwork.core.spigot.config;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.ChatColor;

@AllArgsConstructor
public class ReloadConfigCommand {

    private CorePlugin plugin;

    @Command(name = "reloadcoreconfig", aliases = {"rcc"}, permission = "core.command.config")
    public void reloadConfig(CommandArgs args) {
        args.getSender().sendMessage(ChatColor.RED + "Config reloaded!");
        plugin.reloadConfig();
        return;
    }

}
