package com.alniconetwork.core.bungee.events;

import com.alniconetwork.core.bungee.CorePlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class ConnectionListeners implements Listener {

    public HashMap<UUID, String> fromServer = new HashMap<>();

    @EventHandler
    public void onServerConnect(ServerConnectEvent e) {
        if (e.getPlayer().getServer() != null && e.getTarget() != null) {
            if (fromServer.containsKey(e.getPlayer().getUniqueId())) {
                fromServer.remove(e.getPlayer().getUniqueId());
            }

            fromServer.put(e.getPlayer().getUniqueId(), e.getPlayer().getServer().getInfo().getName());
        }
    }

    @EventHandler
    public void onServerConnectFinished(ServerConnectedEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if(fromServer.containsKey(e.getPlayer().getUniqueId())) {
            CorePlugin.getInstance().getProxy().getScheduler().schedule(CorePlugin.getInstance(), new Runnable() {
                @Override
                public void run() {
                    CorePlugin.getInstance().sendCustomData(e.getPlayer(), e.getPlayer().getName(), fromServer.get(p.getUniqueId()));
                    fromServer.remove(e.getPlayer().getUniqueId());
                }
            }, 90L, TimeUnit.MILLISECONDS);
        } else {
            CorePlugin.getInstance().getProxy().getScheduler().schedule(CorePlugin.getInstance(), new Runnable() {
                @Override
                public void run() {
                    CorePlugin.getInstance().sendCustomData(e.getPlayer(), e.getPlayer().getName(), "join");
                }
            }, 50L, TimeUnit.MILLISECONDS);
        }
    }

    @EventHandler
    public void onServerDisconnect(ServerDisconnectEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (e.getPlayer().getServer() != null && e.getTarget() == null) {
            CorePlugin.getInstance().sendCustomData(p, p.getName().toString(), "leave");
        } else if (e.getPlayer().getServer().getInfo().getName().equals(e.getTarget().getName())) {
            CorePlugin.getInstance().sendCustomData(p, p.getName(), "leave");
        }
    }
}
