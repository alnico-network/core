package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class MessageCommand {

    private CorePlugin plugin;

    @Command(name = "message", aliases = {"m", "msg", "whisper", "w", "pm"}, inGameOnly = true)
    public void message(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> <message>");
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        Player player = args.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);
        if (!coreProfile.isPms()) {
            args.getSender().sendMessage(Messages.HAVE_PRIVATE_MESSAGES_DISABLED.getMessage());
            return;
        }
        CoreProfile targetProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);

        if (!targetProfile.isPms()) {
            args.getSender().sendMessage(Messages.PLAYER_HAS_PRIVATE_MESSAGES_DISABLED.getMessage().replace("%player%", target.getName()));
            return;
        }

        coreProfile.setLastMessage(target.getUniqueId());
        targetProfile.setLastMessage(player.getUniqueId());

        String message = StringUtil.buildString(args.getArgs(), 1);
        args.getSender().sendMessage(Messages.MESSAGE_TO.getMessage()
                .replace("%litePrefix%", CC.translate(targetProfile.getRank() == null || targetProfile.getRank().getLitePrefix() == null ? CC.WHITE : targetProfile.getRank().getLitePrefix()))
                .replace("%player%", target.getName())
                .replace("%message%", message));

        target.sendMessage(Messages.MESSAGE_FROM.getMessage()
                .replace("%litePrefix%", CC.translate(coreProfile.getRank() == null || coreProfile.getRank().getLitePrefix() == null ? CC.WHITE : coreProfile.getRank().getLitePrefix()))
                .replace("%player%", args.getSender().getName())
                .replace("%message%", message));
        return;
    }
}
