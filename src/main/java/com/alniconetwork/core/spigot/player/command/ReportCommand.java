package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class ReportCommand {

    private CorePlugin plugin;

    @Command(name = "report", inGameOnly = true)
    public void report(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> <reason>");
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        Player player = args.getPlayer();
        if (player == target) {
            args.getSender().sendMessage(Messages.CANT_REPORT_YOURSELF.getMessage());
            return;
        }
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (System.currentTimeMillis() - coreProfile.getLastReport() <= (1000 * this.plugin.getManagerHandler().getPlayerManager().getReportDelay())) {
            args.getSender().sendMessage(Messages.MUST_WAIT_TO_REPORT.getMessage());
            return;
        }

        CoreProfile targetProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(target);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("server", CorePlugin.getInstance().getConfig().getString("server-name"));
        messageMap.put("rank", coreProfile.getRank().getName());
        messageMap.put("player", player.getName());
        messageMap.put("targetRank", targetProfile.getRank().getName());
        messageMap.put("target", target.getName());
        messageMap.put("message", StringUtil.buildString(args.getArgs(), 1));
        this.plugin.getManagerHandler().getRedisManager().send("report", messageMap);

        args.getSender().sendMessage(Messages.REPORT_SENT.getMessage());
        return;
    }
}
