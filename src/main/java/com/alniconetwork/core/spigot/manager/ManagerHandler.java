package com.alniconetwork.core.spigot.manager;

import com.alniconetwork.core.spigot.grant.GrantManager;
import com.alniconetwork.core.spigot.mongo.MongoManager;
import com.alniconetwork.core.spigot.player.PlayerManager;
import com.alniconetwork.core.spigot.profile.ProfileManager;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.config.ConfigurationManager;
import com.alniconetwork.core.spigot.punishment.PunishmentManager;
import com.alniconetwork.core.spigot.rank.RankManager;
import com.alniconetwork.core.spigot.redis.RedisListener;
import com.alniconetwork.core.spigot.redis.RedisManager;
import com.alniconetwork.core.spigot.staff.StaffManager;

@Getter
@RequiredArgsConstructor
public class ManagerHandler {

    private final CorePlugin plugin;

    private MongoManager mongoManager;
    private RedisManager redisManager;
    private RankManager rankManager;
    private ProfileManager profileManager;
    private PunishmentManager punishmentManager;
    private PlayerManager playerManager;
    private StaffManager staffManager;
    private GrantManager grantManager;
    private ConfigurationManager configurationManager;

    public void register() {
        mongoManager = new MongoManager(this);
        redisManager = new RedisManager(this,
                CorePlugin.getInstance().getConfig().getString("redis.ip"),
                CorePlugin.getInstance().getConfig().getInt("redis.port"),
                30000,
                CorePlugin.getInstance().getConfig().getString("redis.password"));

        redisManager.registerListeners(
                new RedisListener()
        );
        redisManager.initialize();
        rankManager = new RankManager(this);
        profileManager = new ProfileManager(this);
        punishmentManager = new PunishmentManager(this);
        playerManager = new PlayerManager(this);
        staffManager = new StaffManager(this);
        grantManager = new GrantManager(this);
        configurationManager = new ConfigurationManager(this);
    }

}
