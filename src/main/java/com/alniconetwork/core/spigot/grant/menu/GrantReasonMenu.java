package com.alniconetwork.core.spigot.grant.menu;

import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GrantReasonMenu extends Menu {

    private String name;
    private Rank rank;
    private String time;

    private boolean specifiyingReason;
    private String reason = "";

    public GrantReasonMenu(String name, Rank rank, String time) {
        super("Select a grant reason", 9, false);

        this.name = name;
        this.rank = rank;
        this.time = time;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        inventory.setItem(0,
                new ItemBuilder(Material.DIAMOND)
                        .setName(CC.GREEN + "Staff Promotion")
                        .setLore(CC.GRAY + name + " has been promoted to a higher staff role")
                        .toItemStack());

        inventory.setItem(1,
                new ItemBuilder(Material.GOLDEN_SWORD)
                        .setName(CC.RED + "Staff Demotion")
                        .setLore(CC.GRAY + name + " has been demoted as staff")
                        .toItemStack());

        inventory.setItem(2,
                new ItemBuilder(Material.PAPER)
                        .setName(CC.YELLOW + "Buycraft Error")
                        .setLore(CC.GRAY + name + " did not receive a rank from Buycraft")
                        .toItemStack());

        inventory.setItem(3,
                new ItemBuilder(Material.GOLDEN_HELMET)
                        .setName(CC.GOLD + "Giveaway Winner")
                        .setLore(CC.GRAY + name + " has won a rank giveaway")
                        .toItemStack());

        inventory.setItem(4,
                new ItemBuilder(Material.FLINT_AND_STEEL)
                        .setName(CC.PINK + "No reason specified")
                        .setLore(CC.GRAY + "Don't provide a reason for this grant")
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.WRITABLE_BOOK)
                        .setDurability((short) 3)
                        .setName(CC.AQUA + "Custom Reason")
                        .setLore(CC.GRAY + "Specify your own reason for this grant")
                        .toItemStack());
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        if (currentItem.getType() == Material.AIR) return;

        switch (slot) {
            case 0: {
                reason = "Staff Promotion";
                break;
            }
            case 1: {
                reason = "Staff Demotion";
                break;
            }
            case 2: {
                reason = "Buycraft Error";
                break;
            }
            case 3: {
                reason = "Giveaway Winner";
                break;
            }
            case 4: {
                reason = "No reason specified";
                break;
            }
            case 8: {
                player.closeInventory();

                specifiyingReason = true;
                player.sendMessage(Messages.GRANT_SETTING_REASON.getMessage()
                        .replace("%player%", name)
                        .replace("%rank%", rank.getName()));
                break;
            }
        }
        if (slot != 8) {
            player.closeInventory();
            grant();
        }
        menuEvent.setCancelled(true);
    }

    @EventHandler
    public void onChat(PlayerChatEvent event) {
        Player player = event.getPlayer();

        if (player != this.getPlayer()) return;

        if (specifiyingReason) {
            event.setCancelled(true);

            reason = event.getMessage();
            grant();
            specifiyingReason = false;
            return;
        }
    }
    private void grant() {
        getPlayer().performCommand("grant " + name + " " + rank.getName() + " " + time + " " + reason);
    }
}
