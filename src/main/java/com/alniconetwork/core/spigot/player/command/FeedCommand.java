package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class FeedCommand {

    private CorePlugin plugin;

    @Command(name = "feed", permission = "core.command.feed", inGameOnly = true)
    public void feed(CommandArgs args) {
        if (args.length() == 0) {
            Player player = args.getPlayer();

            player.setFoodLevel(20);

            args.getSender().sendMessage(Messages.YOU_HAVE_BEEN_FED.getMessage());
            return;
        }
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        target.setFoodLevel(20);

        args.getSender().sendMessage(Messages.FED_PLAYER.getMessage().replace("%player%", target.getName()));
        target.sendMessage(Messages.YOU_HAVE_BEEN_FED.getMessage());
        return;
    }
}
