package com.alniconetwork.core.spigot.menu;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
public class MenuEvent {

    private final boolean leftClick;
    private final boolean rightClick;
    private final boolean shiftClick;
    private boolean cancelled;

}
