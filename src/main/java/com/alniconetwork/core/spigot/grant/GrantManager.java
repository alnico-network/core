package com.alniconetwork.core.spigot.grant;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GrantManager extends Manager {

    public GrantManager(ManagerHandler managerHandler) {
        super(managerHandler);
    }

    public Grant fromDocument(Document document) {
        Grant grant = new Grant(
                document.get("added-by") != null ? UUID.fromString(document.getString("added-by")) : null,
                document.getLong("date"),
                document.getString("rank"),
                document.getString("time"),
                document.getLong("expire"),
                document.getString("reason"),
                document.getBoolean("active"));
        return grant;
    }

    public Document toDocument(Grant grant) {
        Map<String, Object> documentMap = new HashMap<>();

        documentMap.put("added-by", grant.getAddedBy() == null ? null : grant.getAddedBy().toString());
        documentMap.put("date", grant.getDate());
        documentMap.put("rank", grant.getRank());
        documentMap.put("time", grant.getTime());
        documentMap.put("expire", grant.getExpire());
        documentMap.put("reason", grant.getReason());
        documentMap.put("active", grant.isActive());

        return new Document(documentMap);
    }
}
