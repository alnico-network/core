package com.alniconetwork.core.spigot.util;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;

@AllArgsConstructor
public enum Messages {

    RANK_ALREADY_EXISTS("&cThat rank already exists."),
    RANK_CREATED("&7You have created the rank &3%rank%&7."),
    RANK_DOESNT_EXIST("&cThat rank doesn't exist."),
    RANK_DELETED("&7You have deleted the rank &3%rank%&7."),
    RANK_PREFIX_SET("&7You have set the prefix of &3%rank% &7to &r%prefix%&7."),
    RANK_LITE_PREFIX_SET("&7You have set the lite prefix of &3%rank% &7to &b%litePrefix%&7."),
    RANK_SUFFIX_SET("&7You have set the suffix of &3%rank% &7to &r%suffix%&7."),
    INVALID_WEIGHT("&cPlease enter a valid weight."),
    RANK_WEIGHT_SET("&7You have set the weight of &3%rank% &7to &b%weight%&7."),
    RANK_HAS_PERMISSION("&cThe rank %rank% already has that permission."),
    RANK_PERMISSION_ADDED("&7You have given rank &3%rank% &7the permission &b%permission%&7."),
    RANK_DOESNT_HAVE_PERMISSION("&cThe rank %rank% doesn't have that permission."),
    RANK_PERMISSION_REMOVED("&7You have revoked rank &3%rank% &7of the permission &b%permission%&7."),
    RANK_SETTING_PREFIX("&7Enter the new prefix for the rank &3%rank%&7..."),
    RANK_SETTING_LITE_PREFIX("&7Enter the new lite prefix for the rank &3%rank%&7..."),
    RANK_SETTING_SUFFIX("&7Enter the new suffix for the rank &3%rank%&7..."),
    RANK_SETTING_WEIGHT("&7Enter the new weight for the rank &3%rank%&7..."),
    DEFAULT_RANK_SET("&7The default rank has been set to &3%rank%&7."),
    COULD_NOT_FIND_PLAYER("&cCould not find player with the name %name%."),
    RANK_UPDATED("&7Your rank has been updated to &3%rank%&7."),
    GRANT_SETTING_REASON("&7Enter a reason to grant &3%player% &7the &b%rank% &7rank..."),
    GRANTED_RANK("&7You have granted &3%player% &7the rank &3%rank% &7for &b%reason% &7for &b%time%&7."),
    SETTING_CUSTOM_DURATION("&7Enter the duration you would like to grant &3%player% &7the rank &b%rank% &7for..."),
    DELETED_GRANT("&7You have removed grant &3#%grantNumber% &7from &b%player%&7."),
    BAN_BROADCAST("&a%player% has banned %player1% for %time% for %reason%."),
    BAN_BROADCAST_SILENT("&7(Silent) &a%player% has banned %player1% for %time% for %reason%."),
    KICK_BROADCAST("&a%player% has kicked %player1% for %reason%."),
    KICK_BROADCAST_SILENT("&7(Silent) &a%player% has kick %player1% for %reason%."),
    PLAYER_NOT_BANNED("&c%player% is not banned."),
    PLAYER_NOT_MUTED("&c%player% is not muted."),
    UNBAN_BROADCAST("&a%player% has unbanned %player1% for %reason%."),
    UNBAN_BROADCAST_SILENT("&7(Silent) &a%player% has unbanned %player1% for %reason%."),
    UNBLACKLIST_BROADCAST("&a%player% has unblacklisted %player1% for %reason%."),
    UNBLACKLIST_BROADCAST_SILENT("&7(Silent) &a%player% has unblacklisted %player1% for %reason%."),
    UNMUTE_BROADCAST("&a%player% has unmuted %player1% for %reason%."),
    UNMUTE_BROADCAST_SILENT("&7(Silent) &a%player% has unmuted %player1% for %reason%."),
    MUTE_BROADCAST("&a%player% has muted %player1% for %time% for %reason%."),
    MUTE_BROADCAST_SILENT("&7(Silent) &a%player% has muted %player1% for %time% for %reason%."),
    BLACKLIST_BROADCAST("&a%player% has blacklisted %player1% for for %reason%."),
    BLACKLIST_BROADCAST_SILENT("&7(Silent) &a%player% has blacklisted %player1% for %reason%."),
    YOU_ARE_MUTED("&cYou are muted for: &7%reason%\n&cExpires in: &7%expire%"),
    BROADCAST("&8[&4Alert&8] &r%message%"),
    CHAT_CLEARED("&cChat has been cleared by %player%."),
    YOU_HAVE_BEEN_HEALED("&6You have been healed."),
    YOU_HAVE_BEEN_FED("&6You have been fed."),
    HEALED_PLAYER("&6You have healed &r%player%&6."),
    FED_PLAYER("&6You have fed &r%player%&6."),
    REQUEST("&9[R] &b[%server%] &r%litePrefix%%player% &7has requested assistance\n&9   Reason: &7%reason%"),
    REPORT("&9[R] &b[%server%] &r%litePrefix%%player% &7has reported &r%targetLitePrefix%%target%\n&9   Reason: &7%reason%"),
    REQUEST_SENT("&aWe have received your request."),
    REPORT_SENT("&aWe have received your report."),
    MUST_WAIT_TO_REQUEST("&cYou may only request every 2 minutes."),
    MUST_WAIT_TO_REPORT("&cYou may only report every 2 minutes."),
    CANT_REPORT_YOURSELF("&cYou can't report yourself!"),
    CANT_HOLD_AIR("&cYou are holding air."),
    ITEM_RENAMED("&6The item in your hand has been renamed to &r%name%&6."),
    ALREADY_HAVE_64("&cYou already have 64 of this item."),
    GIVEN_MORE("&6There you go."),
    GAMEMODE_UPDATED("&6Your gamemode has been updated to &r%gamemode%&6."),
    PLAYER_GAMEMODE_UPDATED("&f%player%'s &6gamemode has been updated to &f%gamemode%&6."),
    INVALID_NUMBER("&cPlease enter a valid number."),
    PUBLIC_CHAT_MUTED("&cPublic chat has been muted."),
    PUBLIC_CHAT_ENABLED("&aPublic chat has been unmuted."),
    CHAT_DELAY("&cThe chat delay has been updated to %delay%s."),
    MUST_WAIT_TO_CHAT("&cYou must wait %time%s to chat again."),
    CHAT_IS_MUTED("&cPublic chat is currently muted."),
    LIST("%ranks%\n&7(%online%/%max%) &r%players%"),
    MESSAGE_FROM("&c(From &r%litePrefix%%player%&c) %message%"),
    MESSAGE_TO("&c(To &r%litePrefix%%player%&c) %message%"),
    HAVE_PRIVATE_MESSAGES_DISABLED("&cYou have private messages disabled."),
    PLAYER_HAS_PRIVATE_MESSAGES_DISABLED("&c%player% has private messages disabled."),
    NOT_IN_CONVERSATION("&cYou are not engaged in a conversation."),
    PLAYER_NO_LONGER_ONLINE("&cThat player is no longer online."),
    ENGAGED_IN_CONVERSATION("&eYou are currently engaged in a conversation with &a%player%&e."),
    PRIVATE_MESSAGES_ENABLED("&eYou are now receiving private messages."),
    PRIVATE_MESSAGES_DISABLED("&eYou are no longer receiving private messages."),
    GLOBAL_CHAT_ENABLED("&eYou are now viewing global chat."),
    GLOBAL_CHAT_DISABLED("&eYou are no longer viewing global chat."),
    PING("&a%player%'s &eping is &a%ping%&e."),
    CANT_FREEZE_YOURSELF("&cYou can't freeze yourself."),
    FROZE_PLAYER("&aYou have froze %player%."),
    UNFROZE_PLAYER("&aYou have unfroze %player%."),
    FROZEN("\n \n \n \n&cYou are currently frozen.\nPlease join teamspeak: &7ts.server.com\n \n \n \n"),
    UNFROZEN("&aYou have been unfrozen by a staff member."),
    STAFF_CHAT_ENABLED("&eYou have entered staff chat."),
    STAFF_CHAT_DISABLED("&eYou have exited staff chat."),
    STAFF_JOINED("&3[Staff] &r%litePrefix%%player% &9has joined &b%server%"),
    STAFF_LEFT("&3[Staff] &r%litePrefix%%player% &9has left &b%server%"),
    STAFF_SWITCH("&3[Staff] &r%litePrefix%%player% &9has switched from &b%server% &9to &b%server1%"),
    SENT_COMMAND("&eSent the command &a%command% &eto the server &a%server%&e. If this server exists, the command will be ran.");

    private String message;

    public String getDefaultMessage() {
        return message;
    }

    public String getMessage() {
        return (CorePlugin.getInstance().getManagerHandler().getConfigurationManager().getMessagesFile().get(getPath()) != null ? CC.translate(CorePlugin.getInstance().getManagerHandler().getConfigurationManager().getMessagesFile().getString(getPath())) : CC.translate(message)).replace("%splitter%", "┃");
    }

    public String getPath() {
        return this.toString().replace("_", "-");
    }
}
