package com.alniconetwork.core.spigot.rank.menu;

import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class RankRemovePermissionMenu extends Menu {

    private Rank rank;
    private String permission;

    public RankRemovePermissionMenu(Rank rank, String permission) {
        super("Delete " + permission + "?", 9, false);

        this.rank = rank;
        this.permission = permission;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        inventory.setItem(3,
                new ItemBuilder(Material.LEGACY_WOOL)
                        .setDurability((short) 5)
                        .setName(CC.GREEN + CC.BOLD + "Confirm")
                        .setLore(
                                CC.GRAY + "Delete " + permission + " from " + rank.getName() + ".",
                                CC.GRAY + "This action can NOT be reversed.")
                        .toItemStack());

        inventory.setItem(5,
                new ItemBuilder(Material.LEGACY_WOOL)
                        .setDurability((short) 14)
                        .setName(CC.RED + CC.BOLD + "Cancel")
                        .setLore(CC.GRAY + "Return to " + rank.getName() + " description.")
                        .toItemStack());
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        menuEvent.setCancelled(true);
        switch (slot) {
            case 3 : {
                player.closeInventory();
                player.performCommand("rank delperm " + rank.getName() + " " + permission);
                break;
            }
            case 5 : {
                player.performCommand("rank manage " + rank.getName());
                break;
            }
        }
    }
}
