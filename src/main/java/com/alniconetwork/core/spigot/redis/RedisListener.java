package com.alniconetwork.core.spigot.redis;

import com.alniconetwork.core.spigot.grant.GrantListener;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.punishment.PunishmentListener;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.staff.StaffJoinType;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.Map;

public class RedisListener {

    @RedisHandler("staff-chat")
    public void onStaffChat(Map<String, Object> messageMap) {
        String server = (String) messageMap.get("server");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");
        String message = (String) messageMap.get("message");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffChat(server, rank, player, message);
        return;
    }

    @RedisHandler("staff-join")
    public void onStaffJoin(Map<String, Object> messageMap) {
        String server = (String) messageMap.get("server");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffStatus(server, rank, player, StaffJoinType.JOIN, null);
        return;
    }

    @RedisHandler("staff-leave")
    public void onStaffLeave(Map<String, Object> messageMap) {
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffStatus(null, rank, player, StaffJoinType.QUIT, null);
        return;
    }

    @RedisHandler("staff-switch")
    public void onStaffSwitch(Map<String, Object> messageMap) {
        String server = (String) messageMap.get("server");
        String old = (String) messageMap.get("old");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffStatus(old, rank, player, StaffJoinType.SWITCH, server);
        return;
    }

    @RedisHandler("report")
    public void onReport(Map<String, Object> messageMap) {
        String server = (String) messageMap.get("server");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");
        Rank targetRank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("targetRank"));
        String target = (String) messageMap.get("target");
        String message = (String) messageMap.get("message");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffReport(server, rank, player, targetRank, target, message);
    }

    @RedisHandler("request")
    public void onRequest(Map<String, Object> messageMap) {
        String server = (String) messageMap.get("server");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String player = (String) messageMap.get("player");
        String message = (String) messageMap.get("message");

        CorePlugin.getInstance().getManagerHandler().getStaffManager().sendStaffRequest(server, rank, player, message);
    }

    @RedisHandler("grant")
    public void onGrant(Map<String, Object> messageMap) {
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        Player player = CorePlugin.getInstance().getServer().getPlayer((String) messageMap.get("player"));

        if (player != null) {
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(player);

            coreProfile.getGrantList().clear();
            Bukkit.getScheduler().scheduleAsyncDelayedTask(CorePlugin.getInstance(), () -> {
                GrantListener.fetchGrants(player, coreProfile);
                coreProfile.setRank(rank);
                player.setDisplayName(rank.getLitePrefix() + player.getName());
                player.sendMessage(Messages.RANK_UPDATED.getMessage().replace("%rank%", rank.getName()));
            }, 1L);
        }
    }

    @RedisHandler("punishment")
    public void onPunishment(Map<String, Object> messageMap) {
        String type = (String) messageMap.get("type");
        String reason = (String) messageMap.get("reason");
        if (type.equalsIgnoreCase("blacklist")) {
            String[] ips = ((String) messageMap.get("ips")).split(":");

            for (Player player : CorePlugin.getInstance().getServer().getOnlinePlayers()) {
                CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(player);
                boolean kick = false;
                for (String ip : ips) {
                    if (coreProfile.getIps().contains(ip)) {
                        kick = true;

                    }
                }
                if (kick) {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(CorePlugin.getInstance(), () ->
                            player.kickPlayer(CC.translate(CorePlugin.getInstance().getConfig().getString("punishment.blacklist-format"))
                                    .replace("%reason%", reason)));
                }
            }
            return;
        }
        Player player = CorePlugin.getInstance().getServer().getPlayer((String) messageMap.get("player"));
        long time = Long.parseLong((String) messageMap.get("time"));

        if (player != null) {
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(player);
            switch (type) {
                case "ban": {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(CorePlugin.getInstance(), () ->
                            player.kickPlayer(CC.translate(CorePlugin.getInstance().getConfig().getString(time == -1 ? "punishment.perm-ban-format" : "punishment.temp-ban-format"))
                                    .replace("%reason%", reason)
                                    .replace("%expire%", TimeUtil.formatTimeMillis(time, false))));
                    break;
                }
                case "mute": {
                    coreProfile.getPunishmentList().clear();
                    PunishmentListener.fetchMutes(player);
                    PunishmentListener.fetchBans(player);
                    PunishmentListener.fetchKicks(player);
                    PunishmentListener.fetchBlacklists(player);
                    break;
                }
                case "unmute": {
                    for (Punishment punishment : coreProfile.getPunishmentList())
                        if (punishment.getPunishmentType() == Punishment.PunishmentType.MUTE) {

                            punishment.setActive(false);
                            punishment.setRemovedBy(null);
                            punishment.setRemovalReason(reason);
                            punishment.setRemovalDate(System.currentTimeMillis());
                        }
                    break;
                }
            }
        }
    }

    @RedisHandler("rank")
    public void onRank(Map<String, Object> messageMap) {
        String update = (String) messageMap.get("update");
        Rank rank = CorePlugin.getInstance().getManagerHandler().getRankManager().getRank((String) messageMap.get("rank"));
        String rankName = (rank != null ? rank.getName() : "");
        String thing = messageMap.containsKey("thing") ? (String) messageMap.get("thing") : "";
        switch (update.toLowerCase()) {
            case "permission": {

                String permission = (String) messageMap.get("permission");

                if (thing.equalsIgnoreCase("remove")) {
                    rank.getPermissions().remove(permission);
                } else {
                    rank.getPermissions().add(permission);
                }
                CorePlugin.getInstance().getServer().getOnlinePlayers().forEach(CorePlugin.getInstance().getManagerHandler().getRankManager()::updatePermissions);
                break;
            }
            case "tpermissions": {

                String permission = (String) messageMap.get("tpermission");

                if (thing.equalsIgnoreCase("remove")) {
                    rank.getTownyPermissions().remove(permission);
                } else {
                    rank.getTownyPermissions().add(permission);
                }
                if(CorePlugin.getInstance().getConfig().getString("server-name") == "Towny") {
                    CorePlugin.getInstance().getServer().getOnlinePlayers().forEach(CorePlugin.getInstance().getManagerHandler().getRankManager()::updatePermissions);
                }
                break;
            }
            case "prefix": {
                rank.setPrefix(CC.translate(thing));
                break;
            }
            case "liteprefix": {
                rank.setLitePrefix(CC.translate(thing));
                break;
            }
            case "suffix": {
                rank.setSuffix(CC.translate(thing));
                break;
            }
            case "weight": {
                rank.setWeight(Integer.parseInt(thing));
                break;
            }
            case "delete": {
                CorePlugin.getInstance().getManagerHandler().getRankManager().getRankList().remove(rank);
                break;
            }
            case "create": {
                CorePlugin.getInstance().getManagerHandler().getRankManager().addRank(new Rank((String) messageMap.get("rank")));
                break;
            }
            case "default": {
                CorePlugin.getInstance().getManagerHandler().getRankManager().setDefaultRank(rank);
                break;
            }
        }
        CorePlugin.getInstance().getServer().broadcast(CC.DARK_GRAY + "[" + CC.RED + "Core" + CC.DARK_GRAY + "] " + CC.RESET + "Rank " + rankName + CC.RESET + " Updated.", Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
    }
}
