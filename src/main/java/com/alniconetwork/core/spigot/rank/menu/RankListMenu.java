package com.alniconetwork.core.spigot.rank.menu;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.ColorUtil;
import com.alniconetwork.core.spigot.util.CorrespondingWool;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.Tag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class RankListMenu extends Menu {

    private int page;
    private int previousPage;
    private int maxPages;
    private int nextPage;

    private List<Rank> sortedRanks;

    public RankListMenu(int page) {
        super("Rank List", 27, true);

        this.page = page;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        sortedRanks = new ArrayList<>(CorePlugin.getInstance().getManagerHandler().getRankManager().getRankList());
        sortedRanks.sort(Comparator.comparing(r -> r.getWeight()));

        previousPage = page - 1 <= 1 ? 1 : page - 1;

        maxPages = sortedRanks.size() / 18;
        if (sortedRanks.size() % 18 != 0) {
            maxPages += 1;
        }

        nextPage = page + 1 >= maxPages ? maxPages : page + 1;

        inventory.setItem(0,
                new ItemBuilder(Material.GRAY_CARPET)
                        .setName(CC.GRAY + (previousPage == 1 ? "First Page" : "Page " + previousPage))
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.GRAY_CARPET)
                        .setName(CC.GRAY + (nextPage == maxPages ? "Last Page" : "Page " + nextPage))
                        .toItemStack());

        int start = page * 18 - 18;
        int end = start + 18;

        int slot = 9;

        for (int i = start; i < end; i++) {
            if (i < sortedRanks.size() && sortedRanks.size() > 0) {
                Rank rank = sortedRanks.get(i);

                CorrespondingWool correspondingWool = CorrespondingWool.getByColor(ColorUtil.getChatColor(rank.getLitePrefix()));

                inventory.setItem(slot,
                        new ItemBuilder(Material.LEGACY_WOOL)
                                .setName(rank.getLitePrefix() + rank.getName())
                                .setDurability((short) correspondingWool.getId())
                                .setLore(CC.BLUE + "Click to manage " + rank.getName() + ".")
                                .toItemStack());
            } else {
                inventory.setItem(slot,
                        new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE)
                                .setName(" ")
                                .toItemStack());
            }
            slot += 1;
        }
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        boolean isWool = Tag.WOOL.isTagged(currentItem.getType());
        if (isWool) {

            Rank rank = sortedRanks.get(page * 18 - 27 + slot);

            player.performCommand("rank manage " + rank.getName());
        } else {
            switch (slot) {
                case 0: {
                    if (previousPage != page) {
                        RankListMenu rankListMenu = new RankListMenu(previousPage);

                        rankListMenu.open(player);
                    }
                    break;
                }
                case 8: {
                    if (nextPage != page) {
                        RankListMenu rankListMenu = new RankListMenu(nextPage);

                        rankListMenu.open(player);
                    }
                    break;
                }
            }
        }
        menuEvent.setCancelled(true);
    }
}
