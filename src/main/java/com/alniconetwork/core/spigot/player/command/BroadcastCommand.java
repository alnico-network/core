package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;

@AllArgsConstructor
public class BroadcastCommand {

    private CorePlugin plugin;

    @Command(name = "broadcast", aliases = {"bc", "shout", "alert"}, permission = "core.command.broadcast")
    public void broadcast(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <message>");
            return;
        }
        String message = CC.translate(StringUtil.buildString(args.getArgs(), 0));
        this.plugin.getServer().broadcastMessage(Messages.BROADCAST.getMessage().replace("%message%", message));
        return;
    }

}
