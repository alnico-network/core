package com.alniconetwork.core.spigot.config;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.Getter;
import com.alniconetwork.core.spigot.util.Logger;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Arrays;

@Getter
public class ConfigurationManager extends Manager {

    private YamlConfiguration messagesFile;

    public ConfigurationManager(ManagerHandler managerHandler) {
        super(managerHandler);

        createFiles();
    }

    private void createFiles() {
        try {
            File mFile = new File(managerHandler.getPlugin().getDataFolder(), "messages.yml");
            if (!mFile.exists()) {
                mFile.createNewFile();
                Logger.success("Created messages.yml");
            }
            messagesFile = YamlConfiguration.loadConfiguration(mFile);
            Arrays.stream(Messages.values()).filter(messages -> messagesFile.get(messages.getPath()) == null).forEach(messages -> messagesFile.set(messages.getPath(), messages.getDefaultMessage()));
            saveMessagesFile();
        } catch (Exception ex) {
            Logger.error("Error loading files.");
        }
    }

    public void saveMessagesFile() {
        try {
            File mFile = new File(managerHandler.getPlugin().getDataFolder(), "messages.yml");
            messagesFile.save(mFile);
        } catch (Exception ex) {
            Logger.error("Could not save messages.yml");
        }
    }
}
