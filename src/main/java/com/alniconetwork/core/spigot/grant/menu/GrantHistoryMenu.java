package com.alniconetwork.core.spigot.grant.menu;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.grant.Grant;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GrantHistoryMenu extends Menu {

    private String name;
    private UUID uuid;
    private boolean online;
    private int page;

    private int previousPage;
    private int maxPages;
    private int nextPage;

    private List<Grant> grantList = null;

    public GrantHistoryMenu(String name, UUID uuid, boolean online, int page) {
        super("Grant History of " + name, 18, true);

        this.name = name;
        this.uuid = uuid;
        this.online = online;
        this.page = page;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        if (online) {
            Player target = Bukkit.getPlayer(uuid);
            CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);

            grantList = coreProfile.getGrantList();
        } else {
            grantList = new ArrayList<>();

            MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("grants");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();

                Grant grant = CorePlugin.getInstance().getManagerHandler().getGrantManager().fromDocument(foundDocument);
                grantList.add(grant);
            }
        }
        maxPages = grantList.size() / 9;
        if (grantList.size() % 9 != 0) {
            maxPages += 1;
        }

        previousPage = page - 1 <= 1 ? 1 : page - 1;

        nextPage = page + 1 >= maxPages ? maxPages : page + 1;

        inventory.setItem(0,
                new ItemBuilder(Material.GRAY_CARPET)
                        .setName(CC.GRAY + (previousPage == 1 ? "First Page" : "Page " + previousPage))
                        .toItemStack());

        inventory.setItem(8,
                new ItemBuilder(Material.GRAY_CARPET)
                        .setName(CC.GRAY + (nextPage == maxPages ? "Last Page" : "Page " + nextPage))
                        .toItemStack());

        int start = page * 9 - 9;
        int end = start + 9;

        int slot = 9;

        for (int i = start; i < end; i++) {
            if (i < grantList.size() && grantList.size() > 0) {
                Grant grant = grantList.get(i);

                String grantedBy = grant.getAddedBy() == null ? "CONSOLE" : Bukkit.getOfflinePlayer(grant.getAddedBy()).getName();

                boolean perm = grant.getExpire() < grant.getDate();
                boolean expired = grant.getExpire() > grant.getDate() && grant.getExpire() - System.currentTimeMillis() <= 0L;

                inventory.setItem(slot,
                        new ItemBuilder((grant.isActive() && !expired ? Material.LIME_DYE : Material.GRAY_DYE))
                                .setName(CC.AQUA + "Grant #" + (grantList.indexOf(grant) + 1))
                                .setLore(
                                        CC.GRAY + CC.STRIKE + "---------------------",
                                        CC.GRAY + "Rank: " + CC.AQUA + grant.getRank(),
                                        CC.GRAY + "Reason: " + CC.AQUA + grant.getReason(),
                                        CC.GRAY + "Added: " + CC.AQUA + TimeUtil.formatTimeMillis(System.currentTimeMillis() - grant.getDate(), true) + " ago",
                                        CC.GRAY + "Added by: " + CC.AQUA + grantedBy,
                                        CC.GRAY + "Duration: " + CC.AQUA + grant.getTime(),
                                        CC.GRAY + "Expire" + (expired ? "d" : "s") + ": " + CC.AQUA + (expired ? TimeUtil.formatTimeMillis(System.currentTimeMillis() - grant.getExpire(), true) + " ago" : perm ? "Never" : TimeUtil.formatTimeMillis(grant.getExpire() - System.currentTimeMillis(), true)),
                                        "",
                                        CC.BLUE + "Click to delete this grant",
                                        CC.GRAY + CC.STRIKE + "---------------------"
                                )
                                .toItemStack());

            } else {
                inventory.setItem(slot,
                        new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE)
                                .setName(" ")
                                .toItemStack());
            }
            slot += 1;
        }
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        if (currentItem.getType() == Material.LIME_DYE || currentItem.getType() == Material.GRAY_DYE) {

            int grantNumber = page * 18 - 26 + slot;

            Player target = Bukkit.getPlayer(uuid);
            GrantRemoveGrantMenu grantRemoveGrantMenu = new GrantRemoveGrantMenu(name, uuid, target != null, grantNumber);
            grantRemoveGrantMenu.open(player);
        } else {
            switch (slot) {
                case 0: {
                    if (previousPage != page) {
                        GrantHistoryMenu grantHistoryMenu = new GrantHistoryMenu(name, uuid, Bukkit.getPlayer(uuid) != null, previousPage);

                        grantHistoryMenu.open(player);
                    }
                    break;
                }
                case 8: {
                    if (nextPage != page) {
                        GrantHistoryMenu grantHistoryMenu = new GrantHistoryMenu(name, uuid, Bukkit.getPlayer(uuid) != null, nextPage);

                        grantHistoryMenu.open(player);
                    }
                    break;
                }
            }
            menuEvent.setCancelled(true);
        }
    }
}
