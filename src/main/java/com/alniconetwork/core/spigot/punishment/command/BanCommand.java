package com.alniconetwork.core.spigot.punishment.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class BanCommand {

    private UUID uuid;
    private long time;
    private String reason;
    private String name;

    private final CorePlugin plugin;

    @Command(name = "ban", permission = "core.command.ban")
    public void ban(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> [duration] [-s] [reason]");
            return;
        }
        uuid = null;
        name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        reason = "";
        boolean silent = false;
        time = -1L;
        if (args.length() > 1) {
            time = args.getArgs(1).contains("-s") ? -1l : TimeUtil.parseTime(args.getArgs(1));

            reason = StringUtil.buildString(args.getArgs(), time == -1L ? 1 : 2);

            silent = reason.contains("-s");
            reason = reason.replaceFirst("-s", "");
            reason = reason.replace("  ", " ");
            if (reason.startsWith(" ")) {
                reason = reason.replaceFirst(" ", "");
            }
        }
        if (reason.endsWith(" ")) {
            reason = StringUtil.replaceLast(reason, " ", "");
        }
        if (reason.replace(" ", "").equalsIgnoreCase("")) {
            reason = plugin.getConfig().getString("punishment.default-ban-reason");
        }

        MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("bans");
        CompletableFuture.runAsync(() -> {
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {

                Document foundDocument = (Document) mongoCursor.next();

                Punishment punishment = this.plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);
                if (punishment.isActive()) {
                    punishment.setActive(false);
                    punishment.setRemovedBy(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null);
                    punishment.setRemovalReason("Ban Override");
                    punishment.setRemovalDate(System.currentTimeMillis());
                }
                Document document = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);
                document.put("uuid", uuid.toString());

                mongoCollection.deleteOne(foundDocument);
                mongoCollection.insertOne(document);
            }
        });

        Punishment punishment = new Punishment(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null, Punishment.PunishmentType.BAN, System.currentTimeMillis(), time == -1 ? "Forever" : TimeUtil.formatTimeMillis(time, true), System.currentTimeMillis() + time, reason, true, null, "", 0);

        Document newDocument = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);
        newDocument.put("uuid", uuid.toString());

        CompletableFuture.runAsync(() -> mongoCollection.insertOne(newDocument));

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type", "ban");
        messageMap.put("player", name);
        messageMap.put("reason", reason);
        messageMap.put("time", time);

        this.plugin.getManagerHandler().getRedisManager().send("punishment", messageMap);

        String broadcast = (silent ? Messages.BAN_BROADCAST_SILENT : Messages.BAN_BROADCAST).getMessage()
                .replace("%player%", args.getSender().getName())
                .replace("%player1%", (target == null ? name : target.getName()))
                .replace("%time%", time == -1 ? "forever" : TimeUtil.formatTimeMillis(time, false))
                .replace("%reason%", reason);

        if (silent) {
            this.plugin.getServer().broadcast(broadcast, "core.silent");
            this.plugin.getServer().broadcast(broadcast, Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
        } else
            this.plugin.getServer().broadcastMessage(broadcast);
        return;
    }
}
