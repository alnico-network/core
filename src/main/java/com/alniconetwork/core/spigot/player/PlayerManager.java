package com.alniconetwork.core.spigot.player;

import lombok.Getter;
import lombok.Setter;
import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;

@Getter
@Setter
public class PlayerManager extends Manager {

    private boolean chatMuted;
    private long chatDelay;
    private long reportDelay;
    private long requestDelay;

    public PlayerManager(ManagerHandler managerHandler) {
        super(managerHandler);

        fetch();
    }

    private void fetch() {
        reportDelay = managerHandler.getPlugin().getConfig().getInt("delay.report");
        requestDelay = managerHandler.getPlugin().getConfig().getInt("delay.request");
    }
}
