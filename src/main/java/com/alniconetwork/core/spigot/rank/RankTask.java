package com.alniconetwork.core.spigot.rank;

import com.alniconetwork.core.spigot.grant.Grant;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public class RankTask extends BukkitRunnable {

    private CorePlugin plugin;

    public void run() {
        this.plugin.getServer().getOnlinePlayers().forEach(p -> {
            CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(p);

            if (coreProfile.getGrantList().size() <= 0) {
                return;
            }
            Grant latestGrant = activeGrant(coreProfile);
            if (latestGrant != null && latestGrant.getExpire() > latestGrant.getDate() && latestGrant.getExpire() < System.currentTimeMillis()) {

                Grant lastGrant = latestNonExpiredGrant(coreProfile);
                if (lastGrant != null && this.plugin.getManagerHandler().getRankManager().getRank(lastGrant.getRank()) != null) {
                    coreProfile.setRank(this.plugin.getManagerHandler().getRankManager().getRank(lastGrant.getRank()));
                    p.setDisplayName(this.plugin.getManagerHandler().getRankManager().getRank(lastGrant.getRank()).getLitePrefix() + p.getName());

                    lastGrant.setActive(true);
                }
            } else if (coreProfile.getRank() != null && !coreProfile.getRank().getName().equalsIgnoreCase(latestGrant.getRank())) {
                coreProfile.setRank(this.plugin.getManagerHandler().getRankManager().getRank(latestGrant.getRank()));
                p.setDisplayName(this.plugin.getManagerHandler().getRankManager().getRank(latestGrant.getRank()).getLitePrefix() + p.getName());

            }
        });
    }

    public static Grant activeGrant(CoreProfile coreProfile) {
        List<Grant> grantList = new ArrayList<>(coreProfile.getGrantList());
        Collections.reverse(grantList);

        for (Grant grant : grantList) {
            if (grant.isActive()) {
                return grant;
            }
        }
        return null;
    }


    public static Grant latestNonExpiredGrant(CoreProfile coreProfile) {
        List<Grant> grantList = new ArrayList<>(coreProfile.getGrantList());
        Collections.reverse(grantList);

        for (Grant grant : grantList) {
            if (grant.getExpire() < grant.getDate() || grant.getExpire() > System.currentTimeMillis() && !grant.isActive()) {
                return grant;
            }
        }
        return null;
    }
}
