package com.alniconetwork.core.spigot.player.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RenameCommand {

    @Command(name = "rename", permission = "core.command.rename", inGameOnly = true)
    public void rename(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <name>");
            return;
        }
        Player player = args.getPlayer();
        if (player.getItemInHand().getType() == Material.AIR) {
            args.getSender().sendMessage(Messages.CANT_HOLD_AIR.getMessage());
            return;
        }
        String newName = CC.translate(StringUtil.buildString(args.getArgs(), 0));
        ItemStack newItem = new ItemBuilder(player.getItemInHand()).setName(newName).toItemStack();

        player.setItemInHand(newItem);
        player.updateInventory();

        args.getSender().sendMessage(Messages.ITEM_RENAMED.getMessage().replace("%name%", newName));
        return;
    }
}
