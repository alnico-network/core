package com.alniconetwork.core.spigot.grant.menu;

import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.grant.Grant;
import com.alniconetwork.core.spigot.menu.Menu;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.rank.RankTask;
import com.alniconetwork.core.spigot.util.ItemBuilder;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.alniconetwork.core.spigot.menu.MenuEvent;
import com.alniconetwork.core.spigot.util.CC;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class GrantRemoveGrantMenu extends Menu {

    private String name;
    private UUID uuid;
    private boolean online;
    private int grantNumber;

    public GrantRemoveGrantMenu(String name, UUID uuid, boolean online, int grantNumber) {
        super("Delete grant #" + grantNumber + "?", 9, false);

        this.name = name;
        this.uuid = uuid;
        this.online = online;
        this.grantNumber = grantNumber;
    }

    @Override
    public void build() {
        Inventory inventory = getInventory();

        inventory.setItem(3,
                new ItemBuilder(Material.LIME_WOOL)
                        .setName(CC.GREEN + CC.BOLD + "Confirm")
                        .setLore(
                                CC.GRAY + "Delete grant " + grantNumber + " from " + name + ".",
                                CC.GRAY + "This action can NOT be reversed.")
                        .toItemStack());

        inventory.setItem(5,
                new ItemBuilder(Material.RED_WOOL)
                        .setName(CC.RED + CC.BOLD + "Cancel")
                        .setLore(CC.GRAY + "Return to " + name + "'s grant history.")
                        .toItemStack());
    }

    @Override
    public void onClick(Player player, ItemStack currentItem, int slot, MenuEvent menuEvent) {
        menuEvent.setCancelled(true);
        switch (slot) {
            case 3: {
                player.closeInventory();
                if (online) {
                    Player target = Bukkit.getPlayer(uuid);
                    CoreProfile coreProfile = CorePlugin.getInstance().getManagerHandler().getProfileManager().getProfile(target);

                    Grant grant = coreProfile.getGrantList().get(grantNumber - 1);

                    coreProfile.getGrantList().remove(coreProfile.getGrantList().get(grantNumber - 1));

                    if (grant.isActive()) {
                        Grant lastGrant = RankTask.latestNonExpiredGrant(coreProfile);
                        if (lastGrant != null && CorePlugin.getInstance().getManagerHandler().getRankManager().getRank(lastGrant.getRank()) != null) {
                            lastGrant.setActive(true);
                            coreProfile.setRank(CorePlugin.getInstance().getManagerHandler().getRankManager().getRank(lastGrant.getRank()));
                            target.setDisplayName(coreProfile.getRank().getLitePrefix() + target.getName());
                        } else {
                            coreProfile.setRank(CorePlugin.getInstance().getManagerHandler().getRankManager().getDefaultRank());
                            target.setDisplayName(coreProfile.getRank().getLitePrefix() + target.getName());
                        }
                    }
                }
                List<Document> grantList = new ArrayList<>();

                MongoCollection mongoCollection = CorePlugin.getInstance().getManagerHandler().getMongoManager().getCollection("grants");
                MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

                while (mongoCursor.hasNext()) {
                    grantList.add((Document) mongoCursor.next());
                }

                if (grantList.get(grantNumber - 1).getBoolean("active") && grantList.size() > 1) {
                    Grant lastGrant = latestNonExpiredGrant(grantList);

                    if (lastGrant != null && CorePlugin.getInstance().getManagerHandler().getRankManager().getRank(lastGrant.getRank()) != null) {
                        lastGrant.setActive(true);

                        Document foundDocument = latestNonExpiredDocument(grantList);
                        mongoCollection.deleteOne(foundDocument);

                        Document document = CorePlugin.getInstance().getManagerHandler().getGrantManager().toDocument(lastGrant);
                        document.put("uuid", uuid.toString());
                        mongoCollection.insertOne(document);
                    }
                }
                mongoCollection.deleteOne(grantList.get(grantNumber - 1));
                player.sendMessage(Messages.DELETED_GRANT.getMessage()
                        .replace("%grantNumber%", String.valueOf(grantNumber))
                        .replace("%player%", name));
                break;
            }
            case 5: {
                player.performCommand("granthistory " + name);
                break;
            }
        }

    }

    private Grant latestNonExpiredGrant(List<Document> grants) {
        List<Grant> grantList = new ArrayList<>();

        for (Document grant : grants) {
            grantList.add(CorePlugin.getInstance().getManagerHandler().getGrantManager().fromDocument(grant));
        }
        List<Grant> clone = new ArrayList<>(grantList);

        Collections.reverse(grantList);

        for (Grant grant : grantList) {

            if ((grant.getExpire() < grant.getDate() || grant.getExpire() > System.currentTimeMillis()) && grantNumber != clone.indexOf(grant) + 1) {
                return grant;
            }
        }
        return null;
    }

    private Document latestNonExpiredDocument(List<Document> grants) {
        List<Grant> grantList = new ArrayList<>();
        List<Document> newList = new ArrayList<>(grants);

        for (Document grant : grants) {
            grantList.add(CorePlugin.getInstance().getManagerHandler().getGrantManager().fromDocument(grant));
        }
        List<Grant> clone = new ArrayList<>(grantList);

        Collections.reverse(grantList);
        Collections.reverse(newList);

        for (Grant grant : grantList) {

            if ((grant.getExpire() < grant.getDate() || grant.getExpire() > System.currentTimeMillis()) && grantNumber != clone.indexOf(grant) + 1) {
                return newList.get(grantList.indexOf(grant));
            }
        }
        return null;
    }
}
