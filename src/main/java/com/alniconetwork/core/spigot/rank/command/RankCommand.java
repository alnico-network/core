package com.alniconetwork.core.spigot.rank.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.Messages;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.rank.menu.RankDescriptionMenu;
import com.alniconetwork.core.spigot.rank.menu.RankListMenu;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class RankCommand {

    private CorePlugin plugin;

    @Command(name = "rank", permission = "core.command.rank")
    public void rank(CommandArgs args) {
        args.getSender().sendMessage(new String[]{
                CC.AQUA + CC.BOLD + "Rank Commands:",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank create <name> " + CC.GRAY + "- " + CC.RESET + "Create a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank delete <rank> " + CC.GRAY + "- " + CC.RESET + "Delete a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank setPrefix <rank> <prefix> " + CC.GRAY + "- " + CC.RESET + "Set prefix of a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank setLitePrefix <rank> <litePrefix> " + CC.GRAY + "- " + CC.RESET + "Set lite prefix of a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank setSuffix <rank> <suffix> " + CC.GRAY + "- " + CC.RESET + "Set suffix of a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank setWeight <rank> <weight> " + CC.GRAY + "- " + CC.RESET + "Set weight of a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank addPerm <rank> <permission> " + CC.GRAY + "- " + CC.RESET + "Add global permission to a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank addTPerm <rank> <permission> " + CC.GRAY + "- " + CC.RESET + "Add towny permission to a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank delPerm <rank> <permission> " + CC.GRAY + "- " + CC.RESET + "Delete global permission from a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank delTPerm <rank> <permission> " + CC.GRAY + "- " + CC.RESET + "Delete towny permission from a rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank manage <rank> " + CC.GRAY + "- " + CC.RESET + "Manage a rank through GUI",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank setDefault <rank> " + CC.GRAY + "- " + CC.RESET + "Set default rank",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank list " + CC.GRAY + "- " + CC.RESET + "View all ranks in a GUI",
                CC.GRAY + " * " + CC.DARK_AQUA + "/rank save " + CC.GRAY + "- " + CC.RESET + "Forcefully save changes to ranks",
        });
    }

    @Command(name = "rank.create", permission = "core.command.rank")
    public void rankCreate(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <name>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank != null) {
            args.getSender().sendMessage(Messages.RANK_ALREADY_EXISTS.getMessage());
            return;
        }

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "create");
        messageMap.put("rank", name);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_CREATED.getMessage().replace("%rank%", name));
        return;
    }

    @Command(name = "rank.delete", permission = "core.command.rank")
    public void rankDelete(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "delete");
        messageMap.put("rank", name);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_DELETED.getMessage().replace("%rank%", rank.getName()));
        return;
    }

    @Command(name = "rank.setprefix", permission = "core.command.rank")
    public void rankSetPrefix(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <prefix>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String prefix = StringUtil.buildString(args.getArgs(), 1);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "prefix");
        messageMap.put("rank", name);
        messageMap.put("thing", prefix);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_PREFIX_SET.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%prefix%", prefix)
                .replace("%suffix%", rank.getSuffix()));
        return;
    }

    @Command(name = "rank.setliteprefix", permission = "core.command.rank")
    public void rankSetLitePrefix(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <litePrefix>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }

        String litePrefix = StringUtil.buildString(args.getArgs(), 1);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "liteprefix");
        messageMap.put("rank", name);
        messageMap.put("thing", litePrefix);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_LITE_PREFIX_SET.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%litePrefix%", litePrefix));
        return;
    }

    @Command(name = "rank.setsuffix", permission = "core.command.rank")
    public void rankSetSuffix(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <suffix>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }

        String suffix = StringUtil.buildString(args.getArgs(), 1);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "suffix");
        messageMap.put("rank", name);
        messageMap.put("thing", suffix);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_SUFFIX_SET.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%suffix%", suffix));
        return;
    }

    @Command(name = "rank.setweight", permission = "core.command.rank")
    public void rankSetWeight(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <weight>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }

        if (!NumberUtils.isDigits(args.getArgs(1))) {
            args.getSender().sendMessage(Messages.INVALID_WEIGHT.getMessage());
            return;
        }
        int weight = Integer.parseInt(args.getArgs(1));

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "weight");
        messageMap.put("rank", name);
        messageMap.put("thing", weight);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_WEIGHT_SET.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%weight%", String.valueOf(weight)));
        return;
    }

    @Command(name = "rank.addperm", permission = "core.command.rank")
    public void rankAddPerm(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <permission>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String permission = args.getArgs(1);
        if (rank.getPermissions().contains(permission)) {
            args.getSender().sendMessage(Messages.RANK_HAS_PERMISSION.getMessage().replace("%rank%", rank.getName()));
            return;
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "permission");
        messageMap.put("rank", name);
        messageMap.put("thing", "add");
        messageMap.put("permission", permission);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_PERMISSION_ADDED.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%permission%", permission));
        return;
    }

    @Command(name = "rank.addtperm", permission = "core.command.rank")
    public void rankAddTPerm(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <permission>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String permission = args.getArgs(1);
        if (rank.getTownyPermissions().contains(permission)) {
            args.getSender().sendMessage(Messages.RANK_HAS_PERMISSION.getMessage().replace("%rank%", rank.getName()));
            return;
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "tpermissions");
        messageMap.put("rank", name);
        messageMap.put("thing", "add");
        messageMap.put("tpermission", permission);

        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_PERMISSION_ADDED.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%permission%", permission));
        return;
    }

    @Command(name = "rank.delperm", permission = "core.command.rank")
    public void rankDelPerm(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <permission>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String permission = args.getArgs(1);
        if (!rank.getPermissions().contains(permission)) {

            args.getSender().sendMessage(Messages.RANK_DOESNT_HAVE_PERMISSION.getMessage()
                    .replace("%rank%", rank.getName()));
            return;
        }

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "permission");
        messageMap.put("rank", name);
        messageMap.put("thing", "remove");
        messageMap.put("permission", permission);
        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_PERMISSION_REMOVED.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%permission%", permission));
        return;
    }

    @Command(name = "rank.deltperm", permission = "core.command.rank")
    public void rankDelTPerm(CommandArgs args) {
        if (args.length() < 2) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank> <permission>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        String permission = args.getArgs(1);
        if (!rank.getTownyPermissions().contains(permission)) {

            args.getSender().sendMessage(Messages.RANK_DOESNT_HAVE_PERMISSION.getMessage()
                    .replace("%rank%", rank.getName()));
            return;
        }

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "tpermissions");
        messageMap.put("rank", name);
        messageMap.put("thing", "remove");
        messageMap.put("tpermissions", permission);
        this.plugin.getManagerHandler().getRedisManager().send("rank", messageMap);
        args.getSender().sendMessage(Messages.RANK_PERMISSION_REMOVED.getMessage()
                .replace("%rank%", rank.getName())
                .replace("%permission%", permission));
        return;
    }

    @Command(name = "rank.manage", permission = "core.command.rank", inGameOnly = true)
    public void rankManage(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }
        Player player = args.getPlayer();

        RankDescriptionMenu rankDescriptionMenu = new RankDescriptionMenu(rank, 1);
        rankDescriptionMenu.open(player);
        return;
    }

    @Command(name = "rank.setdefault", permission = "core.command.rank")
    public void rankSetDefault(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <rank>");
            return;
        }
        String name = args.getArgs(0);
        Rank rank = this.plugin.getManagerHandler().getRankManager().getRank(name);

        if (rank == null) {
            args.getSender().sendMessage(Messages.RANK_DOESNT_EXIST.getMessage());
            return;
        }

        Rank defaultRank = this.plugin.getManagerHandler().getRankManager().getDefaultRank();

        for (Player player : this.plugin.getServer().getOnlinePlayers()) {
            CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

            if (coreProfile.getRank() == null || (defaultRank != null && defaultRank == coreProfile.getRank())) {
                coreProfile.setRank(rank);
                player.setDisplayName(rank.getLitePrefix() + player.getName());

                this.plugin.getManagerHandler().getRankManager().updatePermissions(player);
            }
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("update", "default");
        messageMap.put("rank", name);

        args.getSender().sendMessage(Messages.DEFAULT_RANK_SET.getMessage().replace("%rank%", rank.getName()));
        return;
    }

    @Command(name = "rank.list", permission = "core.command.rank", inGameOnly = true)
    public void rankList(CommandArgs args) {
        Player player = args.getPlayer();

        RankListMenu rankListMenu = new RankListMenu(1);
        rankListMenu.open(player);
        return;
    }

    @Command(name = "rank.save", permission = "core.command.rank")
    public void rankSave(CommandArgs args) {

        CorePlugin.getInstance().getManagerHandler().getRankManager().save();
        args.getSender().sendMessage("§cSent save");
        return;
    }
}
