package com.alniconetwork.core.spigot.punishment;

import com.alniconetwork.core.spigot.manager.Manager;
import com.alniconetwork.core.spigot.manager.ManagerHandler;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PunishmentManager extends Manager {

    public PunishmentManager(ManagerHandler managerHandler) {
        super(managerHandler);
    }

    public Punishment fromDocument(Document document) {
        Punishment punishment = new Punishment(
                document.get("added-by") != null ? UUID.fromString(document.getString("added-by")) : null,
                Punishment.PunishmentType.valueOf(document.getString("punishment-type")),
                document.getLong("date"),
                document.getString("time"),
                document.getLong("expire"),
                document.getString("reason"),
                document.getBoolean("active"),
                document.get("removed-by") != null ? UUID.fromString(document.getString("removed-by")) : null,
                document.getString("removal-reason"),
                document.getLong("removal-date"));
        return punishment;
    }

    public Document toDocument(Punishment punishment) {
        Map<String, Object> documentMap = new HashMap<>();

        documentMap.put("added-by", punishment.getAddedBy() == null ? null : punishment.getAddedBy().toString());
        documentMap.put("punishment-type", punishment.getPunishmentType().toString());
        documentMap.put("date", punishment.getDate());
        documentMap.put("time", punishment.getTime());
        documentMap.put("expire", punishment.getExpire());
        documentMap.put("reason", punishment.getReason());
        documentMap.put("active", punishment.isActive());
        documentMap.put("removed-by", punishment.getRemovedBy() == null ? null : punishment.getRemovedBy().toString());
        documentMap.put("removal-reason", punishment.getRemovalReason());
        documentMap.put("removal-date", punishment.getRemovalDate());

        return new Document(documentMap);
    }
}
