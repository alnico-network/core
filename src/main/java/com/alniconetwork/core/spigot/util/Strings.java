package com.alniconetwork.core.spigot.util;

import java.util.Iterator;
import static java.util.Arrays.*;

public class Strings {

    /**
     * Gives a string consisting of the elements of a given array of strings, each separated by a given separator
     * string.
     *
     * @param pieces the strings to join
     * @param separator the separator
     * @return the joined string
     */
    public static String join( String[] pieces, String separator ) {
        return join( asList( pieces ), separator );
    }

    /**
     * Gives a string consisting of the string representations of the elements of a given array of objects,
     * each separated by a given separator string.
     *
     * @param pieces the elements whose string representations are to be joined
     * @param separator the separator
     * @return the joined string
     */
    public static String join( Iterable<String> pieces, String separator ) {
        StringBuilder buffer = new StringBuilder();

        for (Iterator<String> iter = pieces.iterator(); iter.hasNext(); ) {
            buffer.append( iter.next() );

            if ( iter.hasNext() )
                buffer.append( separator );
        }

        return buffer.toString();
    }


}
