package com.alniconetwork.core.spigot.punishment;

import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.Messages;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.TimeUtil;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.concurrent.CompletableFuture;

public class PunishmentListener implements Listener {

    private static CorePlugin plugin = CorePlugin.getInstance();

    @EventHandler
    public void onPreLogin(AsyncPlayerPreLoginEvent event) {
        MongoCollection mongoCollection = plugin.getManagerHandler().getMongoManager().getCollection("bans");
        MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", event.getUniqueId().toString())).iterator();

        while (mongoCursor.hasNext()) {
            Document foundDocument = (Document) mongoCursor.next();

            Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

            if (punishment.isActive()) {
                if ((punishment.getDate() > punishment.getExpire() || punishment.getExpire() > System.currentTimeMillis())) {
                    event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED,
                            CC.translate(plugin.getConfig().getString(punishment.getDate() > punishment.getExpire() ? "punishment.perm-ban-format" : "punishment.temp-ban-format"))
                                    .replace("%reason%", punishment.getReason())
                                    .replace("%expire%", TimeUtil.formatTimeMillis(punishment.getExpire() - System.currentTimeMillis(), false)));
                } else {
                    punishment.setActive(false);
                    punishment.setRemovalDate(punishment.getExpire());
                    Document document = plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);

                    document.put("uuid", event.getUniqueId().toString());
                    mongoCollection.deleteOne(foundDocument);
                    mongoCollection.insertOne(document);
                }
            }
        }
        MongoCollection blacklistCollection = plugin.getManagerHandler().getMongoManager().getCollection("blacklists");
        MongoCursor blacklistCursor = blacklistCollection.find(Filters.eq("ip", event.getAddress().getHostAddress())).iterator();

        while (blacklistCursor.hasNext()) {
            Document foundDocument = (Document) blacklistCursor.next();

            Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

            if (punishment.isActive()) {
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED,
                        CC.translate(plugin.getConfig().getString("punishment.blacklist-format"))
                                .replace("%reason%", punishment.getReason())
                                .replace("%expire%", TimeUtil.formatTimeMillis(punishment.getExpire() - System.currentTimeMillis(), false)));
            }
        }
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();

        CompletableFuture.runAsync(() -> {
            fetchBans(player);
            fetchMutes(player);
            fetchKicks(player);
            fetchBlacklists(player);
        });
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        CoreProfile coreProfile = plugin.getManagerHandler().getProfileManager().getProfile(player);

        for (Punishment punishment : coreProfile.getPunishmentList()) {
            if (punishment.getPunishmentType() == Punishment.PunishmentType.MUTE) {
                if (punishment.isActive()) {
                    if (punishment.getDate() > punishment.getExpire() || punishment.getExpire() > System.currentTimeMillis()) {
                        event.setCancelled(true);

                        player.sendMessage(Messages.YOU_ARE_MUTED.getMessage()
                                .replace("%reason%", punishment.getReason())
                                .replace("%expire%", punishment.getExpire() < punishment.getDate() ? "Never" : TimeUtil.formatTimeMillis(punishment.getExpire() - System.currentTimeMillis(), false)));
                    }
                } else {
                    punishment.setActive(false);
                    punishment.setRemovalDate(punishment.getExpire());
                }
            }
        }
    }

    public static void fetchBans(Player player) {
        MongoCollection mongoCollection = plugin.getManagerHandler().getMongoManager().getCollection("bans");
        MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).iterator();

        while (mongoCursor.hasNext()) {
            Document foundDocument = (Document) mongoCursor.next();

            Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

            CoreProfile coreProfile = plugin.getManagerHandler().getProfileManager().getProfile(player);
            coreProfile.getPunishmentList().add(punishment);
        }
    }

    public static void fetchMutes(Player player) {
        MongoCollection mongoCollection = plugin.getManagerHandler().getMongoManager().getCollection("mutes");
        MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).iterator();

        while (mongoCursor.hasNext()) {
            Document foundDocument = (Document) mongoCursor.next();
            Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

            CoreProfile coreProfile = plugin.getManagerHandler().getProfileManager().getProfile(player);
            coreProfile.getPunishmentList().add(punishment);
        }
    }

    public static void fetchKicks(Player player) {
        MongoCollection mongoCollection = plugin.getManagerHandler().getMongoManager().getCollection("kicks");
        MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).iterator();

        while (mongoCursor.hasNext()) {
            Document foundDocument = (Document) mongoCursor.next();
            Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

            CoreProfile coreProfile = plugin.getManagerHandler().getProfileManager().getProfile(player);
            coreProfile.getPunishmentList().add(punishment);
        }
    }

    public static void fetchBlacklists(Player player) {
        MongoCollection mongoCollection = plugin.getManagerHandler().getMongoManager().getCollection("blacklists");
        CoreProfile coreProfile = plugin.getManagerHandler().getProfileManager().getProfile(player);

        for (String ip : coreProfile.getIps()) {
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("ip", ip)).iterator();

            while (mongoCursor.hasNext()) {
                Document foundDocument = (Document) mongoCursor.next();
                Punishment punishment = plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);

                if (!coreProfile.getPunishmentList().contains(punishment))
                    coreProfile.getPunishmentList().add(punishment);
            }
        }
    }
}
