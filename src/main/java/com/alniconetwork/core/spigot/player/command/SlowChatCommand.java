package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import org.apache.commons.lang.math.NumberUtils;

@AllArgsConstructor
public class SlowChatCommand {

    private CorePlugin plugin;

    @Command(name = "slowchat", aliases = {"chatslow, chatdelay", "delaychat"}, permission = "core.command.slowchat")
    public void slowChat(CommandArgs args) {
        if (args.length() != 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <delay>");
            return;
        }
        if (!NumberUtils.isDigits(args.getArgs(0))) {
            args.getSender().sendMessage(Messages.INVALID_NUMBER.getMessage());
            return;
        }
        int delay = Integer.parseInt(args.getArgs(0));
        this.plugin.getManagerHandler().getPlayerManager().setChatDelay(delay);

        this.plugin.getServer().broadcastMessage(Messages.CHAT_DELAY.getMessage().replace("%delay%", String.valueOf(delay)));
        return;
    }
}
