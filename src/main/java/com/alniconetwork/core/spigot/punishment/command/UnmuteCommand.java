package com.alniconetwork.core.spigot.punishment.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.RequiredArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bson.Document;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class UnmuteCommand {

    private UUID uuid;
    private String reason;
    private boolean foundPunishment;
    private String name;

    private final CorePlugin plugin;

    @Command(name = "unmute", permission = "core.command.unmute")
    public void unmute(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <player> [duration] [-s] [reason]");
            return;
        }
        uuid = null;
        name = args.getArgs(0);
        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            OfflinePlayer offlinePlayer = this.plugin.getServer().getOfflinePlayer(args.getArgs(0));

            if (offlinePlayer != null && offlinePlayer.getUniqueId() != null) {
                uuid = offlinePlayer.getUniqueId();
            }
        } else {
            uuid = target.getUniqueId();
            name = target.getName();
        }
        if (uuid == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        reason = "";
        reason = StringUtil.buildString(args.getArgs(), 1);

        boolean silent = reason.contains("-s");
        reason = reason.replaceFirst("-s", "");
        reason = reason.replace("  ", " ");
        if (reason.startsWith(" ")) {
            reason = reason.replaceFirst(" ", "");
        }
        if (reason.endsWith(" ")) {
            reason = StringUtil.replaceLast(reason, " ", "");
        }
        if (reason.replace(" ", "").equalsIgnoreCase("")) {
            reason = plugin.getConfig().getString("punishment.default-unmute-reason");
        }

        foundPunishment = false;
        CompletableFuture.runAsync(() -> {

            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("type", "unmute");
            messageMap.put("player", name);
            messageMap.put("reason", reason);
            messageMap.put("time", -1L);

            this.plugin.getManagerHandler().getRedisManager().send("punishment", messageMap);

            MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("mutes");
            MongoCursor mongoCursor = mongoCollection.find(Filters.eq("uuid", uuid.toString())).iterator();

            while (mongoCursor.hasNext()) {

                Document foundDocument = (Document) mongoCursor.next();

                Punishment punishment = this.plugin.getManagerHandler().getPunishmentManager().fromDocument(foundDocument);
                if (punishment.isActive()) {
                    foundPunishment = true;

                    punishment.setActive(false);
                    punishment.setRemovedBy(args.getSender() instanceof Player ? args.getPlayer().getUniqueId() : null);
                    punishment.setRemovalReason(reason);
                    punishment.setRemovalDate(System.currentTimeMillis());

                    Document document = this.plugin.getManagerHandler().getPunishmentManager().toDocument(punishment);

                    document.put("uuid", uuid.toString());

                    mongoCollection.deleteOne(foundDocument);
                    mongoCollection.insertOne(document);
                }
            }

            if (!foundPunishment) {
                args.getSender().sendMessage(Messages.PLAYER_NOT_MUTED.getMessage().replace("%player%", name));
                return;
            }
            String broadcast = (silent ? Messages.UNMUTE_BROADCAST_SILENT : Messages.UNMUTE_BROADCAST).getMessage()
                    .replace("%player%", args.getSender().getName())
                    .replace("%player1%", (target == null ? name : target.getName()))
                    .replace("%reason%", reason);

            if (silent) {
                this.plugin.getServer().broadcast(broadcast, "core.silent");
                this.plugin.getServer().broadcast(broadcast, Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
            } else
                this.plugin.getServer().broadcastMessage(broadcast);
        });
        return;
    }
}
