package com.alniconetwork.core.spigot.profile;

import com.alniconetwork.core.spigot.grant.Grant;
import lombok.Getter;
import lombok.Setter;
import com.alniconetwork.core.spigot.punishment.Punishment;
import com.alniconetwork.core.spigot.rank.Rank;
import org.bukkit.permissions.PermissionAttachment;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class CoreProfile {

    private Rank rank;
    private List<Grant> grantList = new ArrayList<>();
    private List<Punishment> punishmentList = new ArrayList<>();
    private long lastChat;
    private boolean pms = true;
    private UUID lastMessage;
    private long lastReport;
    private long lastRequest;
    private boolean globalChat = true;
    private boolean frozen;
    private boolean staffChat;
    private boolean sentStaffJoin;
    private List<String> ips = new ArrayList<>();
    private PermissionAttachment permissionAttachment;
}
