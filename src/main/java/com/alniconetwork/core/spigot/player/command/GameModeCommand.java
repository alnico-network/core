package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

@AllArgsConstructor
public class GameModeCommand {

    private CorePlugin plugin;

    @Command(name = "gamemode", aliases = {"gm"}, permission = "core.command.gamemode", inGameOnly = true)
    public void gamemode(CommandArgs args) {
        args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <gamemode>");
        return;
    }

    @Command(name = "gamemode.s", aliases = {"gms", "gm0", "gm.s", "gm.0", "gamemode.0"}, permission = "core.command.gamemode", inGameOnly = true)
    public void survival(CommandArgs args) {
        Player player = args.getPlayer();

        if (args.length() == 0) {
            player.setGameMode(GameMode.SURVIVAL);

            args.getSender().sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Survival"));
            return;
        }

        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        target.setGameMode(GameMode.SURVIVAL);

        args.getSender().sendMessage(Messages.PLAYER_GAMEMODE_UPDATED.getMessage().replace("%player%", player.getName()).replace("%gamemode%", "Survival"));
        target.sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Survival"));
        return;
    }

    @Command(name = "gamemode.c", aliases = {"gmc", "gm1", "gm.c", "gm.1", "gamemode.1"}, permission = "core.command.gamemode", inGameOnly = true)
    public void creative(CommandArgs args) {
        Player player = args.getPlayer();

        if (args.length() == 0) {
            player.setGameMode(GameMode.CREATIVE);

            args.getSender().sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Creative"));
            return;
        }

        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        target.setGameMode(GameMode.CREATIVE);

        args.getSender().sendMessage(Messages.PLAYER_GAMEMODE_UPDATED.getMessage().replace("%player%", player.getName()).replace("%gamemode%", "Creative"));
        target.sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Creative"));
        return;
    }

    @Command(name = "gamemode.a", aliases = {"gma", "gm2", "gm.a", "gm.2", "gamemode.2"}, permission = "core.command.gamemode", inGameOnly = true)
    public void adventure(CommandArgs args) {
        Player player = args.getPlayer();

        if (args.length() == 0) {
            player.setGameMode(GameMode.ADVENTURE);

            args.getSender().sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Adventure"));
            return;
        }

        Player target = this.plugin.getServer().getPlayer(args.getArgs(0));
        if (target == null) {
            args.getSender().sendMessage(Messages.COULD_NOT_FIND_PLAYER.getMessage().replace("%name%", args.getArgs(0)));
            return;
        }
        target.setGameMode(GameMode.ADVENTURE);

        args.getSender().sendMessage(Messages.PLAYER_GAMEMODE_UPDATED.getMessage().replace("%player%", player.getName()).replace("%gamemode%", "Adventure"));
        target.sendMessage(Messages.GAMEMODE_UPDATED.getMessage().replace("%gamemode%", "Adventure"));
        return;
    }
}
