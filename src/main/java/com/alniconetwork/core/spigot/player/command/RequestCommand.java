package com.alniconetwork.core.spigot.player.command;

import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.profile.CoreProfile;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.Messages;
import com.alniconetwork.core.spigot.util.StringUtil;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class RequestCommand {

    private CorePlugin plugin;

    @Command(name = "request", aliases = {"helpop"}, inGameOnly = true)
    public void request(CommandArgs args) {
        if (args.length() < 1) {
            args.getSender().sendMessage(CC.RED + "Usage: /" + args.getLabel() + " <reason>");
            return;
        }
        Player player = args.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (System.currentTimeMillis() - coreProfile.getLastRequest() <= (1000 * this.plugin.getManagerHandler().getPlayerManager().getRequestDelay())) {
            args.getSender().sendMessage(Messages.MUST_WAIT_TO_REQUEST.getMessage());
            return;
        }

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("server", CorePlugin.getInstance().getConfig().getString("server-name"));
        messageMap.put("rank", coreProfile.getRank().getName());
        messageMap.put("player", player.getName());
        messageMap.put("message", StringUtil.buildString(args.getArgs(), 0));
        this.plugin.getManagerHandler().getRedisManager().send("request", messageMap);

        args.getSender().sendMessage(Messages.REQUEST_SENT.getMessage());
        return;
    }
}
