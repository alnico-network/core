package com.alniconetwork.core.spigot.player.command;

import com.alniconetwork.core.spigot.command.Command;
import com.alniconetwork.core.spigot.command.CommandArgs;
import com.alniconetwork.core.spigot.util.Messages;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class MoreCommand {

    @Command(name = "more", permission = "core.command.more", inGameOnly = true)
    public void more(CommandArgs args) {
        Player player = args.getPlayer();

        if (player.getItemInHand().getType() == Material.AIR) {
            args.getSender().sendMessage(Messages.CANT_HOLD_AIR.getMessage());
            return;
        }
        if (player.getItemInHand().getAmount() >= 64) {
            args.getSender().sendMessage(Messages.ALREADY_HAVE_64.getMessage());
            return;
        }
        player.getItemInHand().setAmount(64);
        player.updateInventory();

        args.getSender().sendMessage(Messages.GIVEN_MORE.getMessage());
        return;
    }
}
