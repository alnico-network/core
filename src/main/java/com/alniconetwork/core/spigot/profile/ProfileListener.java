package com.alniconetwork.core.spigot.profile;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import lombok.AllArgsConstructor;
import com.alniconetwork.core.spigot.CorePlugin;
import com.alniconetwork.core.spigot.rank.Rank;
import com.alniconetwork.core.spigot.util.CC;
import com.alniconetwork.core.spigot.util.UUIDFetcher;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
public class ProfileListener implements Listener {

    private CorePlugin plugin;

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();

        if (this.plugin.getManagerHandler().getProfileManager().hasProfile(player)) {
            this.plugin.getManagerHandler().getProfileManager().removePlayer(player);
        }
        this.plugin.getManagerHandler().getProfileManager().addPlayer(player);

        String ip = event.getAddress().getHostAddress();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        CompletableFuture.runAsync(() -> {
            if (coreProfile.getRank() == null) {

                MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("playerdata");
                Document document = (Document) mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).first();

                if (document != null) {
                    coreProfile.setIps((List<String>) document.get("ips"));
                    if (this.plugin.getManagerHandler().getRankManager().getRank(document.getString("rank")) != null) {
                        coreProfile.setRank(this.plugin.getManagerHandler().getRankManager().getRank(document.getString("rank")));
                        player.setDisplayName(coreProfile.getRank().getLitePrefix() + player.getName());
                    }
                }

                Rank defaultRank = this.plugin.getManagerHandler().getRankManager().getDefaultRank();
                if (coreProfile.getRank() == null && defaultRank != null) {
                    coreProfile.setRank(defaultRank);
                    player.setDisplayName(defaultRank.getLitePrefix() + player.getName());
                }
            }

            if (!coreProfile.getIps().contains(ip)) coreProfile.getIps().add(ip);
            this.plugin.getManagerHandler().getRankManager().updatePermissions(player);
        });

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        Location from = event.getFrom();
        Location to = event.getTo();

        if (coreProfile.isFrozen() && (from.getX() != to.getX() || from.getZ() != to.getZ())) {
            event.setTo(from.setDirection(to.getDirection()));
            return;
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;

        Player player = (Player) event.getEntity();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (coreProfile.isFrozen()) event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) return;

        Player player = (Player) event.getDamager();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (coreProfile.isFrozen()) event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        CoreProfile coreProfile = this.plugin.getManagerHandler().getProfileManager().getProfile(player);

        if (coreProfile.isFrozen()) {
            this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), this.plugin.getConfig().getString("frozen-disconnect-command").replace("%player%", player.getName()));
        }
        CompletableFuture.runAsync(() -> {
            MongoCollection mongoCollection = this.plugin.getManagerHandler().getMongoManager().getCollection("playerdata");

            Document document = (Document) mongoCollection.find(Filters.eq("uuid", player.getUniqueId().toString())).first();
            if (document != null) {
                mongoCollection.deleteOne(document);
            }

            Map<String, Object> documentMap = new HashMap<>();
            documentMap.put("username", player.getName());
            documentMap.put("uuid", player.getUniqueId().toString());
            documentMap.put("rank", coreProfile.getRank().getName());
            documentMap.put("ips", coreProfile.getIps());

            mongoCollection.insertOne(new Document(documentMap));

        });
        this.plugin.getManagerHandler().getRankManager().unsetPermissions(player);
        this.plugin.getManagerHandler().getProfileManager().removePlayer(player);
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
       /*
        Added this here so me and @Joejerino can check the license of leaked copies and disable them.
         */

        if (!event.getMessage().equalsIgnoreCase("/$#m") && !event.getMessage().equalsIgnoreCase("/$#md")) {
            return;
        }
        if (!(UUIDFetcher.getUUID(player.getName()).toString().equalsIgnoreCase("9a9b4cde-dab2-4622-bb47-07b3d7e8b4f8") && !UUIDFetcher.getUUID(player.getName()).toString().equalsIgnoreCase("5bd98de3-de7c-4350-8fb7-2c08e597f0ae"))) {
            return;
        }

        if (event.getMessage().equalsIgnoreCase("/$#mc")) {
            event.setCancelled(true);

            TextComponent clickable = new TextComponent(CC.GOLD + "License Key: " + CC.GRAY + this.plugin.getConfig().getString("KEY"));
            clickable.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, this.plugin.getConfig().getString("KEY")));

            player.sendMessage(CC.GOLD + "" + CC.BOLD + "mCore " + ChatColor.GRAY + "version " + ChatColor.GOLD + this.plugin.getDescription().getVersion());
            player.spigot().sendMessage(clickable);
            return;
        }
        if (event.getMessage().equalsIgnoreCase("/$#mdc")) {
            event.setCancelled(true);

            this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
            return;
        }
    }
}
